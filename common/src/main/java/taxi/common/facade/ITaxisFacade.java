package taxi.common.facade;

import bee.error.BusinessException;
import bee.session.ExecutionContext;

import bee.datastorage.model.storage.Node;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface ITaxisFacade {
    UUID asociarConductor(Node input, ExecutionContext ec) throws BusinessException;

    UUID servicioAsociarConductor(Node input, ExecutionContext ec) throws BusinessException;

    UUID crearUsuario(Node input, ExecutionContext ec) throws BusinessException;

    UUID crearPosicion(Node input, ExecutionContext ec) throws BusinessException;

    List<Node> consultarIncidentes(Node input, ExecutionContext executionContext) throws BusinessException;

    List<Node> consultarServicios(Node input, ExecutionContext executionContext) throws BusinessException;

    List<Node> consultarVehiculosDisponibles(Node input, ExecutionContext executionContext) throws BusinessException;
}
