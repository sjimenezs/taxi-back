package taxi.common.facade;

import bee.error.BusinessException;
import bee.session.ExecutionContext;

import java.util.List;
import java.util.Map;

public interface ISecurityFacade {
    List<Map> consultarCompaniaPorUsuario(ExecutionContext executionContext) throws BusinessException;
}
