package taxi.common.indexer;

import bee.datastorage.IDataStorageFacade;
import bee.datastorage.INodeIndexer;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class VehiculoIndexer implements INodeIndexer {
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String index(Node input, ExecutionContext ec) throws BusinessException {

        // convierto a JSON

        var storage = Registry.getInstance(IDataStorageFacade.class);

        var vehiculo = storage.getNodeInstanceById(input.getNodeName(),input.getId(),ec);
        var response = mapper.createObjectNode();
        var placa = (String) vehiculo.getCurrentValue("placa", ec);
        var numero = (String) vehiculo.getCurrentValue("numero", ec);
        var disponibilidad = (String) vehiculo.getCurrentValue("disponibilidad", ec);
        var estado = (String) vehiculo.getCurrentValue("estado", ec);

        response.put("placa", placa);
        response.put("numero", numero);
        response.put("disponibilidad", disponibilidad);
        response.put("estado", estado);

        try {
            return mapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            throw new BusinessException(e.getMessage());
        }
    }
}
