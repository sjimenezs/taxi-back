package taxi.common.indexer;

import bee.datastorage.IDataStorageFacade;
import bee.datastorage.INodeIndexer;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IncidenteIndexer implements INodeIndexer {
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String index(Node input, ExecutionContext ec) throws BusinessException {

        // convierto a JSON

        var storage = Registry.getInstance(IDataStorageFacade.class);

        var indidente = storage.getNodeInstanceById(input.getNodeName(),input.getId(),ec);
        var response = mapper.createObjectNode();
        var tipoincidente = (String) indidente.getCurrentValue("tipoincidente", ec);
        var fechaincidente = (String) indidente.getCurrentValue("fechaincidente", ec);

        response.put("tipoincidente", tipoincidente);
        response.put("fechaincidente", fechaincidente);

        var vehiculoRel = indidente.getReverseRelationByName("vehiculo", ec);
        if (vehiculoRel == null) {
            return null;
        }

        var vehiculo = vehiculoRel.getSource();
        if (vehiculo != null) {
            response.put("idvehiculo", vehiculo.getId()+"");
        }

        try {
            return mapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            throw new BusinessException(e.getMessage());
        }
    }
}
