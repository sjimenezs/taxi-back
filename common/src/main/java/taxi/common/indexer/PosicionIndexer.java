package taxi.common.indexer;

import bee.datastorage.IDataStorageFacade;
import bee.datastorage.INodeIndexer;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class PosicionIndexer implements INodeIndexer {
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String index(Node input, ExecutionContext ec) throws BusinessException {

        // convierto a JSON

        var posicion = input;
        var response = mapper.createObjectNode();
        var latitud = (String) posicion.getCurrentValue("latitud", ec);
        var longitud = (String) posicion.getCurrentValue("longitud", ec);

        var fecha = (ZonedDateTime) ZonedDateTime.now(ZoneId.systemDefault());
        String fechaFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(fecha);
        response.put("fecha", fechaFormat);

        var location = mapper.createObjectNode();
        location.put("lat", latitud);
        location.put("lon", longitud);

        response.put("location", location);

        System.out.println(posicion.getCurrentValue("latitud", ec) + "  LUGAR " + longitud);

        var vehiculoRel = posicion.getReverseRelationByName("vehiculo", ec);
        if (vehiculoRel == null) {
            return null;
        }

        var vehiculo = vehiculoRel.getSource();
        if (vehiculo != null) {
            response.put("idvehiculo", vehiculo.getId()+"");
            response.put("estado", (String) vehiculo.getCurrentValue("disponibilidad", ec));
        }

        try {
            return mapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            throw new BusinessException(e.getMessage());
        }
    }
}
