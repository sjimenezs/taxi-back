package taxi.common.indexer;

import bee.datastorage.IDataStorageFacade;
import bee.datastorage.INodeIndexer;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UsuarioIndexer implements INodeIndexer {
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String index(Node input, ExecutionContext ec) throws BusinessException {

        // convierto a JSON

        var storage = Registry.getInstance(IDataStorageFacade.class);

        var usuario = storage.getNodeInstanceById(input.getNodeName(),input.getId(),ec);
        var response = mapper.createObjectNode();
        var nombreUsuario = (String) usuario.getCurrentValue("usuario", ec);

        response.put("usuario", nombreUsuario);

        var personaRel = usuario.getDirectRelationByName("persona", ec);
        if (personaRel == null) {
            return null;
        }

        var persona = personaRel.getTarget();
        if (persona != null) {
            response.put("idpersona", persona.getId().toString());
        }

        try {
            return mapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            throw new BusinessException(e.getMessage());
        }
    }
}