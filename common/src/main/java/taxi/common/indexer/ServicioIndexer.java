package taxi.common.indexer;

import bee.datastorage.IDataStorageFacade;
import bee.datastorage.INodeIndexer;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ServicioIndexer implements INodeIndexer {
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String index(Node input, ExecutionContext ec) throws BusinessException {

        // convierto a JSON

        var storage = Registry.getInstance(IDataStorageFacade.class);

        var servicio = storage.getNodeInstanceById(input.getNodeName(),input.getId(),ec);
        var response = mapper.createObjectNode();
        var longitudorigen = (String) servicio.getCurrentValue("longitudorigen", ec);
        var latitudorigen = (String) servicio.getCurrentValue("latitudorigen", ec);
        var estado = (String) servicio.getCurrentValue("estado", ec);
        var fecha = (ZonedDateTime) servicio.getCurrentValue("fecha", ec);
        String fechaFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(fecha);

        response.put("idservicio", input.getId().toString());
        response.put("longitudorigen", longitudorigen);
        response.put("latitudorigen", latitudorigen);
        response.put("estado", estado);
        response.put("fecha", fechaFormat);

        var personaRel = servicio.getReverseRelationByName("persona", ec);
        if (personaRel == null) {
            return null;
        }

        var persona = personaRel.getSource();
        if (persona != null) {
            var nombre = (String) persona.getCurrentValue("nombre", ec);

            var personaNaturalRel = persona.getDirectRelationByName("persona_natural", ec);

            if (personaNaturalRel == null) {
                return null;
            }

            var personaNatural = personaNaturalRel.getTarget();
            if (personaNatural != null) {
                nombre += (String) bee.utils.StringUtils.coalesceConcat((String) personaNatural.getCurrentValue("apellido", ec)," ","");
            }
            response.put("nombreusuario", nombre);
            response.put("idusuario", persona.getId().toString());
        }

        var vehiculoRel = servicio.getReverseRelationByName("vehiculo", ec);
        if (vehiculoRel != null) {
            var vehiculo = vehiculoRel.getSource();
            if (vehiculo != null) {
                var placa = (String) vehiculo.getCurrentValue("placa", ec);
                var numero = (String) vehiculo.getCurrentValue("numero", ec);

                response.put("placa", placa);
                response.put("numerotaxi", numero);

            }
        }

        var conductorRel = servicio.getDirectRelationByName("conductor", ec);
        if (conductorRel != null) {
            var conductor = conductorRel.getTarget();

            if(conductor != null){
                var nombre = (String) conductor.getCurrentValue("nombre", ec);

                var personaNaturalRel = conductor.getDirectRelationByName("persona_natural", ec);

                if (personaNaturalRel == null) {
                    return null;
                }

                var personaNatural = personaNaturalRel.getTarget();
                if (personaNatural != null) {
                    nombre += (String) bee.utils.StringUtils.coalesceConcat((String) personaNatural.getCurrentValue("segundonombre", ec)," ","");
                    nombre += (String) bee.utils.StringUtils.coalesceConcat((String) personaNatural.getCurrentValue("apellido", ec)," ","");
                    nombre += (String) bee.utils.StringUtils.coalesceConcat((String) personaNatural.getCurrentValue("segundoapellido", ec)," ","");
                }
                response.put("nombretaxista", nombre);
                response.put("idtaxista", conductor.getId().toString());
            }
        }
        try {
            return mapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            throw new BusinessException(e.getMessage());
        }
    }
}