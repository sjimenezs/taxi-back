package taxi.common.indexer;

import bee.datastorage.IDataStorageFacade;
import bee.datastorage.INodeIndexer;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CalificacionIndexer implements INodeIndexer {
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String index(Node input, ExecutionContext ec) throws BusinessException {

        // convierto a JSON

        var storage = Registry.getInstance(IDataStorageFacade.class);

        var calificacion = storage.getNodeInstanceById(input.getNodeName(),input.getId(),ec);
        var response = mapper.createObjectNode();
        var puntaje = (String) calificacion.getCurrentValue("puntaje", ec).toString();

        response.put("puntaje", puntaje);

        var servicioRel = calificacion.getReverseRelationByName("servicio", ec);
        if (servicioRel == null) {
            return null;
        }

        var servicio = servicioRel.getSource();
        if (servicio != null) {
            response.put("idservicio", servicio.getId()+"");
        }

        try {
            return mapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            throw new BusinessException(e.getMessage());
        }
    }
}
