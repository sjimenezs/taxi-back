package taxi.common.indexer;

import bee.datastorage.IDataStorageFacade;
import bee.datastorage.INodeIndexer;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class PersonaIndexer implements INodeIndexer {
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String index(Node input, ExecutionContext ec) throws BusinessException {

        // convierto a JSON

        var storage = Registry.getInstance(IDataStorageFacade.class);

        var persona = storage.getNodeInstanceById(input.getNodeName(),input.getId(),ec);
        var response = mapper.createObjectNode();
        var nombre = (String) persona.getCurrentValue("nombre", ec);
        response.put("tipopersona", (String)persona.getCurrentValue("tipopersona", ec));

        var personaNaturalRel = persona.getDirectRelationByName("persona_natural", ec);

        if (personaNaturalRel == null) {
            return null;
        }

        var personaNatural = personaNaturalRel.getTarget();
        if (personaNatural != null) {
            nombre += (String) bee.utils.StringUtils.coalesceConcat((String) personaNatural.getCurrentValue("segundonombre", ec), " ", "");
            nombre += (String) bee.utils.StringUtils.coalesceConcat((String) personaNatural.getCurrentValue("apellido", ec)," ","");
            nombre += (String) bee.utils.StringUtils.coalesceConcat((String) personaNatural.getCurrentValue("segundoapellido", ec), " ","");
        }

        response.put("nombrecompleto", nombre);

        var identificacionRel = persona.getDirectRelationByName("identificacion", ec);
        if (identificacionRel != null) {
            var identificacion = identificacionRel.getTarget();
            if (identificacion != null) {
                response.put("identificacion", (String) identificacion.getCurrentValue("identificacion", ec));
            }
        }

        try {
            return mapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            throw new BusinessException(e.getMessage());
        }
    }
}
