package taxi.security.business;

import bee.error.BusinessException;
import bee.session.ExecutionContext;

import java.util.List;
import java.util.Map;

public interface ISecurityBusiness {
    List<Map> consultarCompaniaPorUsuario(ExecutionContext executionContext) throws BusinessException;
}
