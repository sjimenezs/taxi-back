package taxi.security.impl.business;

import bee.configuration.IConfiguration;
import bee.dataaccess.IConnectionFactory;
import bee.dataaccess.util.GenericBusiness;
import bee.datastorage.IDataStorageFacade;
import bee.error.BusinessException;
import bee.error.IExceptionHandler;
import bee.persistence.QueryFilter;
import bee.session.ExecutionContext;
import taxi.security.business.ISecurityBusiness;

import javax.inject.Inject;
import java.util.*;

public class SecurityBusiness implements ISecurityBusiness {
    private final IConfiguration configuration;
    private final IDataStorageFacade dataStorageFacade;
    private final IConnectionFactory connectionFactory;
    private final IExceptionHandler exceptionHandler;

    @Inject
    public SecurityBusiness(IConfiguration configuration, IDataStorageFacade dataStorageFacade, IConnectionFactory connectionFactory, IExceptionHandler exceptionHandler) {
        this.configuration = configuration;
        this.dataStorageFacade = dataStorageFacade;
        this.connectionFactory = connectionFactory;
        this.exceptionHandler = exceptionHandler;
    }

    @Override
    public List<Map> consultarCompaniaPorUsuario(ExecutionContext executionContext) throws BusinessException {
        return GenericBusiness.withConnection((conn) -> {

            var rootContextId = this.configuration.get("app.rootcontext.id", String.class);
            var rootContextUuid = UUID.fromString(rootContextId);
            var rootExecutionContext = executionContext.clone();
            rootExecutionContext.setContextGuid(rootContextUuid);
            rootExecutionContext.setGroupContextGuid(rootContextUuid);


            var data = new ArrayList<QueryFilter>();

            var usuarioFilter = new QueryFilter();
            usuarioFilter.setOperator("=");
            usuarioFilter.setLogic("and");
            usuarioFilter.setValue(executionContext.getUserGuid().toString());
            usuarioFilter.setProperty("idusuario");
            data.add(usuarioFilter);

            var response = this.dataStorageFacade.query("companias_por_usuario", data, rootExecutionContext);


            var res = new ArrayList<Map>();

            if (response != null) {
                for (var companiaUsuario : response) {
                    rootExecutionContext.setContextGuid((UUID) companiaUsuario.get("empresa_id"));
                    rootExecutionContext.setGroupContextGuid((UUID) companiaUsuario.get("empresa_id"));
                    var compania = this.dataStorageFacade.getNodeInstanceById("empresa", (UUID) companiaUsuario.get("empresa_id"), rootExecutionContext);
                    if (compania != null) {

                        var persona = compania.getDirectRelationByName("persona", rootExecutionContext).getTarget();

                        var item = new HashMap<>();
                        item.put("id", compania.getId());
                        item.put("nombre", persona.getCurrentValue("nombre", rootExecutionContext));

                        res.add(item);
                    }
                }
            }

            return res;

        }, executionContext.getConnectionName(), this.connectionFactory, this.exceptionHandler, executionContext);
    }
}
