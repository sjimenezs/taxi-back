package taxi.security.module;

import com.google.inject.AbstractModule;
import taxi.common.facade.ISecurityFacade;
import taxi.security.SecurityFacade;
import taxi.security.business.ISecurityBusiness;
import taxi.security.impl.business.SecurityBusiness;

public class Dependencies extends AbstractModule {
    @Override
    protected void configure() {


        this.bind(ISecurityBusiness.class).to(SecurityBusiness.class);
        this.bind(ISecurityFacade.class).to(SecurityFacade.class);
    }
}
