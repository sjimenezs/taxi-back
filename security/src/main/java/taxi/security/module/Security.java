package taxi.security.module;

import bee.registry.Modulo;
import com.google.inject.AbstractModule;

public class Security extends Modulo {
    @Override
    public String getNombre() {
        return "HUMAN CAPITAL SECURITY";
    }

    @Override
    public AbstractModule getDependencies() {
        return new Dependencies();
    }
}
