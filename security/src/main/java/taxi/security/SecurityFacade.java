package taxi.security;

import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import taxi.common.facade.ISecurityFacade;
import taxi.security.business.ISecurityBusiness;

import java.util.List;
import java.util.Map;

public class SecurityFacade implements ISecurityFacade {
    @Override
    public List<Map> consultarCompaniaPorUsuario(ExecutionContext executionContext) throws BusinessException {
        var business = Registry.getInstance(ISecurityBusiness.class);
        return business.consultarCompaniaPorUsuario(executionContext);
    }
}
