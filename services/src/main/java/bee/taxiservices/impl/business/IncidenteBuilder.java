package bee.taxiservices.impl.business;

import bee.datastorage.ISearchQueryBuilder;
import bee.error.BusinessException;
import bee.persistence.QueryFilter;
import bee.session.ExecutionContext;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class IncidenteBuilder implements ISearchQueryBuilder {

    @Override
    public SearchSourceBuilder build(Map queryDef, List<QueryFilter> queryFilters, Map map1, ExecutionContext executionContext) throws BusinessException {
        SearchSourceBuilder searchRequestBuilder = new SearchSourceBuilder();
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        query.minimumShouldMatch(1);

        searchRequestBuilder.query(query);
        var should = query.should();
        for (QueryFilter filter : queryFilters) {
            should.add(QueryBuilders.matchPhraseQuery(filter.getProperty(), filter.getValue()));
        }
        searchRequestBuilder.size(10000);

        return searchRequestBuilder;
    }
}