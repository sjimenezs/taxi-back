package bee.taxiservices.impl.business;

import bee.configuration.IConfiguration;
import bee.dataaccess.Connection;
import bee.dataaccess.IConnectionFactory;
import bee.dataaccess.SQLConnection;
import bee.dataaccess.util.GenericBusiness;
import bee.datastorage.DataStorageConnection;
import bee.datastorage.IDataStorageFacade;
import bee.datastorage.business.IDataStorageBusiness;
import bee.datastorage.dao.INodeInstanceDAO;
import bee.datastorage.model.MessageSync;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.error.IExceptionHandler;
import bee.fts.IFTSFacade;
import bee.persistence.QueryFilter;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import bee.taxiservices.business.ITaxiBusiness;
import bee.taxiservices.services.base.Relations;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import taxi.common.indexer.PosicionIndexer;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

public class TaxiBusiness implements ITaxiBusiness {

    private final IExceptionHandler exceptionHandler;
    private final IConnectionFactory connectionFactory;
    private final IDataStorageFacade dataStorageFacade;
    private final IDataStorageBusiness dataStorageBusiness;

    private static final String UPDATE_PASSWORD = "Update Password";
    private final IConfiguration configuration;

    @Inject
    public TaxiBusiness(IConfiguration configuration, IExceptionHandler exceptionHandler, IConnectionFactory connectionFactory, IDataStorageFacade dataStorageFacade, IDataStorageBusiness dataStorageBusiness) {

        this.configuration = configuration;
        this.exceptionHandler = exceptionHandler;
        this.connectionFactory = connectionFactory;
        this.dataStorageFacade = dataStorageFacade;
        this.dataStorageBusiness = dataStorageBusiness;
    }

    private Keycloak init(ExecutionContext ec) throws BusinessException {
        var context = (RefreshableKeycloakSecurityContext) ec.getAttributes().get(KeycloakSecurityContext.class.getName());

        AccessToken accessToken = context.getToken();
        String[] ruta = accessToken.getIssuer().split("/realms/");
        var keycloak = KeycloakBuilder.builder().serverUrl(ruta[0]).realm("CabExpress").grantType("password").clientSecret("fcf16fd3-b013-415e-bfcb-2e42a1476cba").clientId("idm-admin").username("idm-admin").password("admin").build();
        return keycloak;
    }

    @Override
    public UUID asociarConductor(Node input, ExecutionContext ec) throws BusinessException {
        return GenericBusiness.withConnection(conn -> {
            var _conn = new DataStorageConnection<SQLConnection>(conn);
            conn.starTransaction();

            UUID id = input.getId();

            dataStorageFacade.persistRelation(_conn, Relations.persona_maneja(input, input.getSourcesByRelationsReverseName("maneja", ec).get(0)), ec);
            dataStorageFacade.commitTransaction(_conn, ec);

            //MessageSync msg = new MessageSync();
            //msg.setNodeName("conductores");
            //msg.setNodeId(input.getId());
            //dataStorageFacade.syncSearchNodeInstance(msg, ec);

            return input.getId();
        }, ec.getConnectionName(), this.connectionFactory, this.exceptionHandler, ec);
    }

    @Override
    public UUID servicioAsociarConductor(Node input, ExecutionContext ec) throws BusinessException {
        return GenericBusiness.withConnection(conn -> {
            var _conn = new DataStorageConnection<SQLConnection>(conn);
            conn.starTransaction();

            dataStorageFacade.persistRelation(_conn, Relations.servicio_vehiculo(input, input.getReverseRelationByName("vehiculo", ec).getSource()), ec);
            dataStorageFacade.commitTransaction(_conn, ec);

            dataStorageFacade.persistRelation(_conn, Relations.servicio_conductor(input, input.getDirectRelationByName("conductor", ec).getTarget()), ec);
            dataStorageFacade.commitTransaction(_conn, ec);

            return input.getId();
        }, ec.getConnectionName(), this.connectionFactory, this.exceptionHandler, ec);
    }

    @Override
    public UUID crearUsuario(Node input, ExecutionContext ec) throws BusinessException {
        try {
            var keycloak = init(ec);
            RealmResource realmResource = keycloak.realm("CabExpress");
            var userRessource = realmResource.users();

            var contactoRelation = input.getDirectRelationByName("contacto", ec);
            if (contactoRelation == null) {
                throw new BusinessException("no esta la info completa:" + "contacto");
            }

            var contacto = contactoRelation.getTarget();
            System.out.println(contacto);
            String usuario = contacto.getData().get("email").get(0).getValue().toString();

            var users = realmResource.users().search(usuario);

            //verifico si el usuario existe
            if (users != null && users.size() > 0) {
                System.out.println("el usuario existe.");
            }

            UserRepresentation newUser = new UserRepresentation();
            newUser.setEmailVerified(false);
            newUser.setEnabled(true);


            var persona_natural = input.getDirectRelationByName("persona_natural", ec);
            if (persona_natural == null) {
                throw new BusinessException("no esta la info completa:" + "contacto");
            }

            String password = null;
            var pn = persona_natural.getTarget();
            if (pn.getData().get("foto") != null) {
                String pass = pn.getData().get("foto").get(0).getValue().toString();
                password = pass;
            } else {
                password = generatePassayPassword();
            }

            List<CredentialRepresentation> credentials = new LinkedList<CredentialRepresentation>();
            CredentialRepresentation credential = new CredentialRepresentation();
            credential.setType(CredentialRepresentation.PASSWORD);

            credential.setValue(password);
            credentials.add(credential);
            newUser.setCredentials(credentials);

            newUser.setUsername(usuario);
            newUser.setFirstName("");
            newUser.setLastName("");
            newUser.setEmail(usuario);
            newUser.setRequiredActions(Arrays.asList(UPDATE_PASSWORD));

            Response response = userRessource.create(newUser);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            IOUtils.copy(((InputStream) response.getEntity()), bos);
            System.out.println(new String(bos.toByteArray()));

            if (response.getStatus() != 201) {

            }

            //obtener el id del usuario

            var location = response.getHeaderString("location");
            if (location.indexOf('/') == -1) {

            }

            var id = location.substring(location.lastIndexOf('/') + 1);

            System.out.println(id);

            return GenericBusiness.withConnection(conn -> {
                var _conn = new DataStorageConnection<SQLConnection>(conn);
                conn.starTransaction();

                var compania = consultarCompaniaPorId((UUID) ec.getContextGuid(), ec);
                if (compania == null) {

                }

                //creo la persona

                var usuarioNode = new Node();
                usuarioNode.setId(UUID.fromString(id));
                usuarioNode.addProperty("estado", "ACTIVE");
                usuarioNode.addProperty("fecharegistro", ec.getDateTime());
                usuarioNode.addProperty("usuario", usuario);
                usuarioNode.addProperty("correo", usuario);
                usuarioNode.addProperty("idusuario", id.toString());
                usuarioNode.setNodeName("usuario");

                input.addRelation(Relations.persona_usuario(input, usuarioNode));
                this.dataStorageFacade.persist(_conn, input, ec);
                conn.commitTransaction();

                MessageSync msg2 = new MessageSync();
                msg2.setNodeName("persona");
                msg2.setNodeId(input.getId());
                dataStorageFacade.syncSearchNodeInstance(msg2, ec);

                conn.commitTransaction();

                MessageSync msg = new MessageSync();
                msg.setNodeName("usuario");
                msg.setNodeId(usuarioNode.getId());
                dataStorageFacade.syncSearchNodeInstance(msg, ec);

                conn.commitTransaction();

                return input.getId();
            }, ec.getConnectionName(), this.connectionFactory, this.exceptionHandler, ec);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UUID crearPosicion(Node input, ExecutionContext ec) throws BusinessException {
        try {

            if (input.getId() == null) {
                input.setId(UUID.randomUUID());
            }

            var indexer = new PosicionIndexer();
            var index = indexer.index(input, ec);

            IFTSFacade iftsFacade = Registry.getInstance(IFTSFacade.class);
            iftsFacade.index(input.getNodeName(), input.getId().toString(), index, ec);

            return input.getId();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Node> consultarIncidentes(Node input, ExecutionContext executionContext) {

        System.out.println(executionContext);

        List<QueryFilter> queryFilters = new ArrayList<>();
        QueryFilter qf = new QueryFilter();
        qf.setLogic("and");
        qf.setOperator("equals");
        qf.setProperty("tipoincidente");
        qf.setValue(input.getCurrentValue("tipoincidente", executionContext).toString());
        queryFilters.add(qf);

        Map queryDef = new HashMap();
        queryDef.put("name", "incidente_buscar");
        queryDef.put("type", "search");
        queryDef.put("querybuilder", "bee.taxiservices.impl.business.IncidenteBuilder");
        queryDef.put("node", "incidente");

        ArrayList<Map> results = new ArrayList<>();
        Map result = new LinkedHashMap();
        result.put("field", "id");
        result.put("type", "INSTANCE");
        result.put("node", "incidente");
        results.add(result);
        queryDef.put("results", results);

        try {
            List<Node> retorno = new ArrayList<>();
            List<Map> resultQuery = dataStorageBusiness.search(queryDef, queryFilters, null, executionContext);
            if (resultQuery != null) {
                for (Map mapQuery : resultQuery) {
                    Node nodeAux = dataStorageBusiness.getNodeInstanceById("incidente", UUID.fromString(mapQuery.get("id").toString()), executionContext);

                    Long time = Long.parseLong(nodeAux.getCurrentValue("fechaincidente", executionContext).toString());

                    Calendar fechaActual = Calendar.getInstance();
                    fechaActual.setTime(new Date());
                    fechaActual.set(Calendar.MINUTE, (fechaActual.get(Calendar.MINUTE) - 5));

                    Calendar fechaEval = Calendar.getInstance();
                    fechaEval.setTimeInMillis(time * 1000);

                    if (fechaEval.compareTo(fechaActual) > 0) {
                        retorno.add(nodeAux);
                    }
                }
            }
            return retorno;
        } catch (BusinessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Node> consultarServicios(Node input, ExecutionContext executionContext) {
        System.out.println("LLEGO");
        QueryFilter filterLatitude = new QueryFilter();
        filterLatitude.setProperty("lat");
        filterLatitude.setValue(input.getCurrentValue("latitudorigen", executionContext).toString());

        QueryFilter filterLongitude = new QueryFilter();
        filterLongitude.setProperty("lon");
        filterLongitude.setValue(input.getCurrentValue("longitudorigen", executionContext).toString());

        QueryFilter filterDistance = new QueryFilter();
        filterDistance.setProperty("dist");
        filterDistance.setValue("1000");

        QueryFilter filterState = new QueryFilter();
        filterState.setProperty("estado");
        filterState.setValue("DISPONIBLE");

        List<QueryFilter> queryFilters = new ArrayList<>();
        queryFilters.add(filterLatitude);
        queryFilters.add(filterLongitude);
        queryFilters.add(filterDistance);
        queryFilters.add(filterState);

        Map queryDef = new HashMap();
        queryDef.put("name", "servicio_buscar");
        queryDef.put("type", "search");
        queryDef.put("querybuilder", "bee.taxiservices.impl.business.ServicioBuilder");
        queryDef.put("node", "posicion");

        Map result = new LinkedHashMap();
        result.put("field", "id");
        result.put("type", "INSTANCE");
        result.put("node", "vehiculo");

        List<Map> results = new ArrayList<>();
        results.add(result);

        queryDef.put("results", results);

        try {
            List<Node> retorno = new ArrayList<>();
            List<Map> resultQuery = dataStorageBusiness.search(queryDef, queryFilters, null, executionContext);
            List<ServicioDTO> servicios = new ArrayList<>();
            if (resultQuery != null) {
                AtomicInteger order = new AtomicInteger();
                GenericBusiness.withConnection(conn -> {
                    var _conn = new DataStorageConnection<SQLConnection>(conn);
                    conn.starTransaction();
                    int contador = 0;

                    for (Map mapQuery : resultQuery) {

                        if (contador != 4) {
                            ServicioDTO servicio = new ServicioDTO();
                            Node nodeAuxPos = dataStorageBusiness.getNodeInstanceById("posicion", UUID.fromString(mapQuery.get("id").toString()), executionContext);
                            Node nodeAuxVehiculo = nodeAuxPos.getReverseRelationByName("vehiculo", executionContext).getSource();

                            // PENDIENTE
                            // RECHAZADO
                            // ACEPTADO
                            // VENCIDO
                            var vehServNode = new Node();
                            vehServNode.setId(UUID.randomUUID());
                            vehServNode.addProperty("estado", "PENDIENTE");
                            vehServNode.addProperty("orden", Long.parseLong(order.getAndIncrement() + ""));
                            vehServNode.setNodeName("vehiculoservicio");
                            this.dataStorageFacade.persist(_conn, vehServNode, executionContext);

                            Node v = new Node();
                            v.setId(nodeAuxVehiculo.getId());
                            v.setNodeName("vehiculo");
                            dataStorageFacade.persistRelation(_conn, Relations.vehiculoservicio_vehiculo(vehServNode, v), executionContext);

                            Node s = new Node();
                            s.setId(input.getId());
                            s.setNodeName("servicio");
                            dataStorageFacade.persistRelation(_conn, Relations.vehiculoservicio_servicio(vehServNode, s), executionContext);
                            dataStorageFacade.commitTransaction(_conn, executionContext);

                            servicio.setVehivulo(nodeAuxVehiculo);
                            servicio.setServicio(input);
                            servicio.setExecutionContext(executionContext);
                            servicio.setDataStorageFacade(dataStorageFacade);
                            servicios.add(servicio);

                            retorno.add(nodeAuxVehiculo);

                            contador++;
                        } else {
                            break;
                        }
                    }
                }, executionContext.getConnectionName(), this.connectionFactory, this.exceptionHandler, executionContext);
                buscarServicio(servicios, 0, input, executionContext);
            }

            return retorno;
        } catch (BusinessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Node> consultarVehiculosDisponibles(Node input, ExecutionContext executionContext) {

        QueryFilter filterState = new QueryFilter();
        filterState.setProperty("disponibilidad");
        filterState.setValue("DISPONIBLE");

        List<QueryFilter> queryFilters = new ArrayList<>();
        queryFilters.add(filterState);

        Map queryDef = new HashMap();
        queryDef.put("name", "vehiculo_disponible_buscar");
        queryDef.put("type", "search");
        queryDef.put("querybuilder", "bee.taxiservices.impl.business.VehiculoDisponibleBuilder");
        queryDef.put("node", "vehiculo");

        Map result = new LinkedHashMap();
        result.put("field", "id");
        result.put("type", "INSTANCE");
        result.put("node", "vehiculo");

        List<Map> results = new ArrayList<>();
        results.add(result);

        queryDef.put("results", results);

        try {
            List<Node> retorno = new ArrayList<>();
            List<Map> resultQuery = dataStorageBusiness.search(queryDef, queryFilters, null, executionContext);

            if (resultQuery != null) {
                AtomicInteger order = new AtomicInteger();
                GenericBusiness.withConnection(conn -> {
                    var _conn = new DataStorageConnection<SQLConnection>(conn);
                    conn.starTransaction();


                    for (Map mapQuery : resultQuery) {
                        Node node = dataStorageBusiness.getNodeInstanceById("vehiculo", UUID.fromString(mapQuery.get("id").toString()), executionContext);
                        retorno.add(node);
                    }
                }, executionContext.getConnectionName(), this.connectionFactory, this.exceptionHandler, executionContext);
            }

            return retorno;
        } catch (BusinessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Node consultarCompaniaPorId(UUID id, ExecutionContext executionContext) throws BusinessException {
        var rootContextId = this.configuration.get("app.rootcontext.id", String.class);
        var rootContextUuid = UUID.fromString(rootContextId);
        var rootExecutionContext = executionContext.clone();
        rootExecutionContext.setContextGuid(rootContextUuid);
        return this.dataStorageFacade.getNodeInstanceById("compania", id, executionContext);
    }

    public String generatePassayPassword() {
        return "Asdf1234$";
    }

    /**
     * Permite hacer peticiones Rest
     *
     * @param url
     * @param method
     * @param body
     * @return
     */
    public static JsonNode executeRequest(String url, String method, String body) {
        JsonNode jsonResponse = null;
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpUriRequest request = null;
        try {
            switch (method) {
                case "POST":
                    request = new HttpPost(url);
                    ((HttpPost) request).setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));
                    break;
                case "GET":
                    request = new HttpGet(url);
                    break;
                default:
                    request = new HttpPost(url);
            }
            request.setHeader("Authorization", "key=AAAA-qeRq0A:APA91bFJu4nFMmNImtA0lkkI9dL8hly9ax19zMhh6ese4BB3hmN-T0q0BMVlrvu9h2UfS1VJe0ZvTX9B4uNoBh1OZl0R9gBQVJ1OZPtOAYhlBWTG8diS4riSM-CYDNEcfHW8nPA6E208");
            request.addHeader("Content-Type", "application/json");

            HttpResponse response = client.execute(request);
            ObjectMapper objectMapper = new ObjectMapper();
            if (response.getEntity() == null) {
                String json = "ok";
                jsonResponse = objectMapper.readTree(json);
            } else {
                jsonResponse = objectMapper.readTree(EntityUtils.toString(response.getEntity()));
            }
        } catch (IOException e) {

        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return jsonResponse;
    }

    private boolean buscarServicio(List<ServicioDTO> listaServicios, int posicion, Node servicio, ExecutionContext executionContext) {
        ExecutorService service = Executors.newFixedThreadPool(1);

        // Manda push
        System.out.println(posicion + " Posi");
        Future<Boolean> taskFTP = service.submit(new BuscarServicio(listaServicios.get(posicion)));

        try {
            Boolean result = taskFTP.get();
            service.shutdownNow();
            if (!result && posicion < listaServicios.size() - 1) {
                buscarServicio(listaServicios, ++posicion, servicio, executionContext);
            } else if (!result && posicion == listaServicios.size() - 1) {
                Node serv = dataStorageBusiness.getNodeInstanceById("servicio", servicio.getId(), executionContext);
                serv.replaceProperty("estado", "Rechazado");
                this.dataStorageFacade.update(serv, executionContext);
                return false;
            } else if (result) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
