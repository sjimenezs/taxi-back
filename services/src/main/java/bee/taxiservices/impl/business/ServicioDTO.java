package bee.taxiservices.impl.business;


import bee.datastorage.IDataStorageFacade;
import bee.datastorage.model.storage.Node;
import bee.session.ExecutionContext;

public class ServicioDTO {

    private Node vehivulo;

    private Node servicio;

    private ExecutionContext executionContext;

    private IDataStorageFacade dataStorageFacade;

    public Node getVehivulo() {
        return vehivulo;
    }

    public void setVehivulo(Node vehivulo) {
        this.vehivulo = vehivulo;
    }

    public Node getServicio() {
        return servicio;
    }

    public void setServicio(Node servicio) {
        this.servicio = servicio;
    }

    public ExecutionContext getExecutionContext() {
        return executionContext;
    }

    public void setExecutionContext(ExecutionContext executionContext) {
        this.executionContext = executionContext;
    }

    public IDataStorageFacade getDataStorageFacade() {
        return dataStorageFacade;
    }

    public void setDataStorageFacade(IDataStorageFacade dataStorageFacade) {
        this.dataStorageFacade = dataStorageFacade;
    }
}
