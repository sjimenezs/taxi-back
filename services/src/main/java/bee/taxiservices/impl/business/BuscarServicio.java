package bee.taxiservices.impl.business;

import bee.datastorage.model.storage.Node;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.concurrent.Callable;

public class BuscarServicio implements Callable<Boolean> {

    ServicioDTO servicioDTO = new ServicioDTO();

    public BuscarServicio(ServicioDTO servicioDTO) {
        this.servicioDTO = servicioDTO;
    }

    @Override
    public Boolean call() throws Exception {

        var idPush = servicioDTO.getVehivulo().getCurrentValue("idpush", servicioDTO.getExecutionContext()).toString();
        System.out.println(idPush + "idPush");
        var title = "Nueva Carrera";
        var body = "Se ha solicitado un nuevo servicio";
        var longitud = servicioDTO.getServicio().getCurrentValue("longitudorigen", servicioDTO.getExecutionContext()).toString();
        var latitud = servicioDTO.getServicio().getCurrentValue("latitudorigen", servicioDTO.getExecutionContext()).toString();
        var idServicio = servicioDTO.getServicio().getId().toString();

        String bodyRequest = String.format("{\"to\":\"%s\",\"data\": {\"title\": \"%s\", \"body\": \"%s\", \"longitud\": %s, \"latitud\": %s, \"servicio\": \"%s\"},\"android\":{\"ttl\":\"0s\"}}", idPush, title,body,longitud,latitud,idServicio);
        executeRequest("https://fcm.googleapis.com/fcm/send", "POST", bodyRequest);

        Thread.sleep(30000);

        Node service = servicioDTO.getDataStorageFacade().getNodeInstanceById("servicio", servicioDTO.getServicio().getId(), servicioDTO.getExecutionContext());

        if(service.getData().get("estado").get(service.getData().get("estado").size()-1).getValue().toString().equalsIgnoreCase("Aceptado") || service.getData().get("estado").get(service.getData().get("estado").size()-1).getValue().toString().equalsIgnoreCase("Finalizado")
                || service.getData().get("estado").get(service.getData().get("estado").size()-1).getValue().toString().equalsIgnoreCase("en Curso")){
            return true;
        }

        return false;
    }

    /**
     * Permite hacer peticiones Rest
     * @param url
     * @param method
     * @param body
     * @return
     */
    public static JsonNode executeRequest(String url, String method, String body) {
        JsonNode jsonResponse = null;
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpUriRequest request = null;
        try {
            switch (method) {
                case "POST":
                    request = new HttpPost(url);
                    ((HttpPost) request).setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));
                    break;
                case "GET":
                    request = new HttpGet(url);
                    break;
                default:
                    request = new HttpPost(url);
            }
            request.setHeader("Authorization", "key=AAAA-qeRq0A:APA91bFJu4nFMmNImtA0lkkI9dL8hly9ax19zMhh6ese4BB3hmN-T0q0BMVlrvu9h2UfS1VJe0ZvTX9B4uNoBh1OZl0R9gBQVJ1OZPtOAYhlBWTG8diS4riSM-CYDNEcfHW8nPA6E208");
            request.addHeader("Content-Type", "application/json");

            HttpResponse response = client.execute(request);
            ObjectMapper objectMapper = new ObjectMapper();
            if (response.getEntity() == null) {
                String json = "ok";
                jsonResponse = objectMapper.readTree(json);
            } else {
                jsonResponse = objectMapper.readTree(EntityUtils.toString(response.getEntity()));
            }
        } catch (IOException e) {

        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return jsonResponse;
    }
}
