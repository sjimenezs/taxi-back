package bee.taxiservices.impl.business;

import bee.datastorage.ISearchQueryBuilder;
import bee.error.BusinessException;
import bee.persistence.QueryFilter;
import bee.session.ExecutionContext;
import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortMode;
import org.elasticsearch.search.sort.SortOrder;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServicioBuilder implements ISearchQueryBuilder {

    @Override
    public SearchSourceBuilder build(Map queryDef, List<QueryFilter> queryFilters, Map map1, ExecutionContext executionContext) throws BusinessException {
        SearchSourceBuilder searchRequestBuilder = new SearchSourceBuilder();
        BoolQueryBuilder query = QueryBuilders.boolQuery();

        searchRequestBuilder.query(query);
        var filter = query.filter();

        Map filterGeo = new HashMap();
        for (QueryFilter dataFilter : queryFilters) {
            switch (dataFilter.getProperty()) {
                case "estado":
                    filter.add(QueryBuilders.matchPhraseQuery(dataFilter.getProperty(), dataFilter.getValue()));
                    break;
                case "lat":
                    filterGeo.put("lat", dataFilter.getValue());
                    break;
                case "lon":
                    filterGeo.put("lon", dataFilter.getValue());
                    break;
                case "dist":
                    filterGeo.put("dist", dataFilter.getValue());
                    break;
            }
        }

        // Filtro Fecha
        filter.add(QueryBuilders.rangeQuery("fecha").from("now-7m"));

        // Filtro Geo
        GeoPoint gp = new GeoPoint();
        gp.resetLat(Double.parseDouble(filterGeo.get("lat").toString()));
        gp.resetLon(Double.parseDouble(filterGeo.get("lon").toString()));

        filter.add(QueryBuilders.geoDistanceQuery("location").point(gp).distance(Double.parseDouble(filterGeo.get("dist").toString()), DistanceUnit.METERS));

        searchRequestBuilder.sort(SortBuilders.geoDistanceSort("location", gp).sortMode(SortMode.MIN).order(SortOrder.ASC).unit(DistanceUnit.METERS).geoDistance(GeoDistance.ARC));
        searchRequestBuilder.size(10000);
        return searchRequestBuilder;
    }
}