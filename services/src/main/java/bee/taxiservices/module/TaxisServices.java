package bee.taxiservices.module;

import bee.datastorage.IDataStorageFacade;
import bee.error.BusinessException;
import bee.registry.Modulo;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import com.google.inject.AbstractModule;
import taxi.common.indexer.*;

public class TaxisServices extends Modulo {
    @Override
    public String getNombre() {
        return "HUMAN CAPITAL SERVICES";
    }

    @Override
    public AbstractModule getDependencies() {
        return new Dependencies();
    }


    @Override
    public void init() throws BusinessException {
        super.init();


        var dataStorageFacade = Registry.getInstance(IDataStorageFacade.class);
        dataStorageFacade.registerIndexer("persona", new PersonaIndexer(), new ExecutionContext());
        dataStorageFacade.registerIndexer("barrio", new BarrioIndexer(), new ExecutionContext());
        dataStorageFacade.registerIndexer("posicion", new PosicionIndexer(), new ExecutionContext());
        dataStorageFacade.registerIndexer("usuario", new UsuarioIndexer(), new ExecutionContext());
        dataStorageFacade.registerIndexer("servicio", new ServicioIndexer(), new ExecutionContext());
        dataStorageFacade.registerIndexer("zona", new ZonaIndexer(), new ExecutionContext());
        dataStorageFacade.registerIndexer("vehiculo", new VehiculoIndexer(), new ExecutionContext());
        dataStorageFacade.registerIndexer("incidente", new IncidenteIndexer(), new ExecutionContext());
        dataStorageFacade.registerIndexer("calificacion", new CalificacionIndexer(), new ExecutionContext());
    }
}
