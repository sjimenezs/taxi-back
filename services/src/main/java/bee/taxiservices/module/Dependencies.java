package bee.taxiservices.module;

import bee.taxiservices.TaxisFacade;
import bee.taxiservices.business.ITaxiBusiness;
import bee.taxiservices.impl.business.TaxiBusiness;
import com.google.inject.AbstractModule;
import taxi.common.facade.ITaxisFacade;

public class Dependencies extends AbstractModule {
    @Override
    protected void configure() {
        this.bind(ITaxiBusiness.class).to(TaxiBusiness.class);
        this.bind(ITaxisFacade.class).to(TaxisFacade.class);
    }
}
