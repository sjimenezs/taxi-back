package bee.taxiservices.services.services;

import bee.datastorage.IDataStorageFacade;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import bee.taxiservices.services.base.servicio_crearBase;
import taxi.common.facade.ITaxisFacade;

public class ServicioAsociarConductor extends servicio_crearBase {

    @Override
    public Node execute(Node input, ExecutionContext executionContext) throws BusinessException {
        var facade = Registry.getInstance(ITaxisFacade.class);
        var dataStorageFacade = Registry.getInstance(IDataStorageFacade.class);
        return dataStorageFacade.getNodeInstanceById("servicio", facade.servicioAsociarConductor(input, executionContext), executionContext);
    }
}