package bee.taxiservices.services.services;

import bee.datastorage.graphql.DataStorageExecutor;
import bee.datastorage.graphql.DataStorageOperation;
import bee.datastorage.graphql.resolver.ListOperationResolver;
import bee.datastorage.model.EStorageStatus;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import bee.taxiservices.services.base.Mappers;
import graphql.schema.*;
import taxi.common.facade.ITaxisFacade;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;

public class ServicioBuscar extends DataStorageExecutor<List<Node>> {

    @Override
    public List<Node> execute(DataFetchingEnvironment dataFetchingEnvironment, ExecutionContext executionContext) throws BusinessException {
        BiFunction<Map, EStorageStatus, Object> mapper = Mappers.mappers.get("servicio_mapper");
        Node input = (Node) mapper.apply((Map)dataFetchingEnvironment.getArgument("input") , EStorageStatus.NEW);
        var facade = Registry.getInstance(ITaxisFacade.class);

        var list = facade.consultarServicios(input, executionContext);
        System.out.println("VEHICULOS SERVICIO: " + list.size());
        return list;
    }

    @Override
    public GraphQLFieldDefinition buildOperationType(DataStorageOperation dataStorageOperation) {
        GraphQLFieldDefinition.Builder type = newFieldDefinition().name(dataStorageOperation.getName());
        type.argument(GraphQLArgument.newArgument().name("input").type(GraphQLTypeReference.typeRef("servicioInput")).build());
        type.type(GraphQLList.list(GraphQLTypeReference.typeRef("servicio")));
        type.dataFetcher(ListOperationResolver.INSTANCE);
        return type.build();
    }
}
