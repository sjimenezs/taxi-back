package bee.taxiservices.services.services;

import bee.datastorage.graphql.DataStorageExecutor;
import bee.datastorage.graphql.DataStorageOperation;
import bee.datastorage.graphql.resolver.CompaniesResolver;
import bee.datastorage.graphql.resolver.ListOperationResolver;
import bee.datastorage.graphql.resolver.MapResolver;
import bee.datastorage.model.EStorageStatus;
import bee.error.BusinessException;
import bee.persistence.GraphQLTypes;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import bee.taxiservices.services.base.Mappers;
import graphql.schema.*;
import taxi.common.facade.ISecurityFacade;

import java.util.*;
import java.util.function.BiFunction;

import bee.datastorage.model.storage.Node;
import taxi.common.facade.ITaxisFacade;

import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLList.list;
import static graphql.schema.GraphQLObjectType.newObject;

public class IncidenteBuscar extends DataStorageExecutor<List<Node>> {

    @Override
    public List<Node> execute(DataFetchingEnvironment dataFetchingEnvironment, ExecutionContext executionContext) throws BusinessException {
        BiFunction<Map, EStorageStatus, Object> mapper = Mappers.mappers.get("incidente_mapper");
        Node input = (Node) mapper.apply((Map)dataFetchingEnvironment.getArgument("input") , EStorageStatus.NEW);
        System.out.println(input);
        var facade = Registry.getInstance(ITaxisFacade.class);

        return facade.consultarIncidentes(input, executionContext);
    }

    @Override
    public GraphQLFieldDefinition buildOperationType(DataStorageOperation dataStorageOperation) {
        GraphQLFieldDefinition.Builder type = newFieldDefinition().name(dataStorageOperation.getName());

        type.argument(GraphQLArgument.newArgument().name("input").type(GraphQLTypeReference.typeRef("incidenteInput")).build());

        type.type(GraphQLList.list(GraphQLTypeReference.typeRef("incidente")));
        type.dataFetcher(ListOperationResolver.INSTANCE);
        return type.build();
    }
}
