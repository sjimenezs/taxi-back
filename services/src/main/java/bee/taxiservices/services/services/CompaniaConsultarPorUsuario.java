package bee.taxiservices.services.services;

import bee.datastorage.graphql.DataStorageExecutor;
import bee.datastorage.graphql.DataStorageOperation;
import bee.datastorage.graphql.resolver.CompaniesResolver;
import bee.datastorage.graphql.resolver.MapResolver;
import bee.error.BusinessException;
import bee.persistence.GraphQLTypes;
import bee.registry.Registry;
import bee.session.ExecutionContext;

import graphql.schema.*;
import taxi.common.facade.ISecurityFacade;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLList.list;
import static graphql.schema.GraphQLObjectType.newObject;

public class CompaniaConsultarPorUsuario extends DataStorageExecutor<List<Map>> {
    @Override
    public List<Map> execute(DataFetchingEnvironment dataFetchingEnvironment, ExecutionContext executionContext) throws BusinessException {
        var facade = Registry.getInstance(ISecurityFacade.class);

        return facade.consultarCompaniaPorUsuario(executionContext);
    }

    @Override
    public List<GraphQLType> buildOutputTypes() throws BusinessException {
        GraphQLObjectType.Builder type = newObject()
                .name("CompaniaConsultarPorUsuarioType");


        GraphQLFieldDefinition.Builder field = newFieldDefinition()
                .name("id");

        field.type(GraphQLTypes.GraphQLID);
        field.dataFetcher(new MapResolver<UUID>("id"));

        type.field(field.build());

        field = newFieldDefinition()
                .name("nombre");

        field.type(GraphQLString);
        field.dataFetcher(new MapResolver<String>("nombre"));

        type.field(field.build());


        return Arrays.asList(type.build());
    }

    @Override
    public GraphQLFieldDefinition buildOperationType(DataStorageOperation operation) {
        GraphQLFieldDefinition.Builder type = newFieldDefinition()
                .name(operation.getName());

        type.type(list(GraphQLTypeReference.typeRef("CompaniaConsultarPorUsuarioType")));

        type.dataFetcher(CompaniesResolver.INSTANCE);

        return type.build();
    }
}
