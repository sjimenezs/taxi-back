package bee.taxiservices.services.services;

import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import bee.taxiservices.services.base.posicion_crearBase;
import taxi.common.facade.ITaxisFacade;


public class PosicionCrear extends posicion_crearBase {

    @Override
    public Node execute(Node input, ExecutionContext executionContext) throws BusinessException {
        var facade = Registry.getInstance(ITaxisFacade.class);
        Node posicion = new Node();
        posicion.setNodeName("posicion");
        posicion.setId(facade.crearPosicion(input, executionContext));
        return posicion;
    }
}
