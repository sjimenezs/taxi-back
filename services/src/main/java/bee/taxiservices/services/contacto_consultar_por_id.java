package bee.taxiservices.services;

import bee.datastorage.IDataStorageFacade;
import bee.datastorage.model.storage.Node;
import bee.taxiservices.services.base.*;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;

import java.util.List;
import java.util.UUID;

public class contacto_consultar_por_id extends contacto_consultar_por_idBase {
    @Override
    public Node execute(UUID input, ExecutionContext executionContext)  throws BusinessException{
        IDataStorageFacade facade = Registry.getInstance(IDataStorageFacade.class);
        return facade.getNodeInstanceById("contacto", input ,executionContext);
    }
}
