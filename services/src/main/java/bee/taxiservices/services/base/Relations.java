package bee.taxiservices.services.base ;

import bee.datastorage.model.storage.NodeProperty;
import bee.datastorage.model.storage.Node;
import bee.datastorage.model.storage.Relation;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.List;

public class Relations {

    public static Relation persona_identificacion(Node persona,  Node identificacion){
    //direct
            var relation = new Relation();
            relation.setSourceNode("persona");
            relation.setTargetNode("identificacion");
            relation.setDirectName("identificacion");
            relation.setReverseName("persona");
            relation.setSource(persona);
            relation.setTarget(identificacion);
            return relation;
    }

    public static Relation identificacion_persona(Node identificacion, Node persona){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("persona");
                relation.setTargetNode("identificacion");
                relation.setDirectName("identificacion");
                relation.setReverseName("persona");
                relation.setSource(persona);
                relation.setTarget(identificacion);
                return relation;
        }

    public static Relation persona_contacto(Node persona,  Node contacto){
    //direct
            var relation = new Relation();
            relation.setSourceNode("persona");
            relation.setTargetNode("contacto");
            relation.setDirectName("contacto");
            relation.setReverseName("persona");
            relation.setSource(persona);
            relation.setTarget(contacto);
            return relation;
    }

    public static Relation contacto_persona(Node contacto, Node persona){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("persona");
                relation.setTargetNode("contacto");
                relation.setDirectName("contacto");
                relation.setReverseName("persona");
                relation.setSource(persona);
                relation.setTarget(contacto);
                return relation;
        }

    public static Relation persona_persona_natural(Node persona,  Node persona_natural){
    //direct
            var relation = new Relation();
            relation.setSourceNode("persona");
            relation.setTargetNode("personanatural");
            relation.setDirectName("persona_natural");
            relation.setReverseName("persona");
            relation.setSource(persona);
            relation.setTarget(persona_natural);
            return relation;
    }

    public static Relation personanatural_persona(Node personanatural, Node persona){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("persona");
                relation.setTargetNode("personanatural");
                relation.setDirectName("persona_natural");
                relation.setReverseName("persona");
                relation.setSource(persona);
                relation.setTarget(personanatural);
                return relation;
        }

    public static Relation persona_vehiculo(Node persona,  Node vehiculo){
    //direct
            var relation = new Relation();
            relation.setSourceNode("persona");
            relation.setTargetNode("vehiculo");
            relation.setDirectName("vehiculo");
            relation.setReverseName("dueno");
            relation.setSource(persona);
            relation.setTarget(vehiculo);
            return relation;
    }

    public static Relation vehiculo_dueno(Node vehiculo, Node dueno){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("persona");
                relation.setTargetNode("vehiculo");
                relation.setDirectName("vehiculo");
                relation.setReverseName("dueno");
                relation.setSource(dueno);
                relation.setTarget(vehiculo);
                return relation;
        }

    public static Relation persona_servicios(Node persona,  Node servicios){
    //direct
            var relation = new Relation();
            relation.setSourceNode("persona");
            relation.setTargetNode("servicio");
            relation.setDirectName("servicios");
            relation.setReverseName("persona");
            relation.setSource(persona);
            relation.setTarget(servicios);
            return relation;
    }

    public static Relation servicio_persona(Node servicio, Node persona){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("persona");
                relation.setTargetNode("servicio");
                relation.setDirectName("servicios");
                relation.setReverseName("persona");
                relation.setSource(persona);
                relation.setTarget(servicio);
                return relation;
        }

    public static Relation persona_turnos(Node persona,  Node turnos){
    //direct
            var relation = new Relation();
            relation.setSourceNode("persona");
            relation.setTargetNode("turno");
            relation.setDirectName("turnos");
            relation.setReverseName("conductor");
            relation.setSource(persona);
            relation.setTarget(turnos);
            return relation;
    }

    public static Relation turno_conductor(Node turno, Node conductor){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("persona");
                relation.setTargetNode("turno");
                relation.setDirectName("turnos");
                relation.setReverseName("conductor");
                relation.setSource(conductor);
                relation.setTarget(turno);
                return relation;
        }

    public static Relation vehiculo_serviciosvehiculo(Node vehiculo,  Node serviciosvehiculo){
    //direct
            var relation = new Relation();
            relation.setSourceNode("vehiculo");
            relation.setTargetNode("servicio");
            relation.setDirectName("serviciosvehiculo");
            relation.setReverseName("vehiculo");
            relation.setSource(vehiculo);
            relation.setTarget(serviciosvehiculo);
            return relation;
    }

    public static Relation servicio_vehiculo(Node servicio, Node vehiculo){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("vehiculo");
                relation.setTargetNode("servicio");
                relation.setDirectName("serviciosvehiculo");
                relation.setReverseName("vehiculo");
                relation.setSource(vehiculo);
                relation.setTarget(servicio);
                return relation;
        }

    public static Relation vehiculo_rodamientos(Node vehiculo,  Node rodamientos){
    //direct
            var relation = new Relation();
            relation.setSourceNode("vehiculo");
            relation.setTargetNode("rodamiento");
            relation.setDirectName("rodamientos");
            relation.setReverseName("vehiculo");
            relation.setSource(vehiculo);
            relation.setTarget(rodamientos);
            return relation;
    }

    public static Relation rodamiento_vehiculo(Node rodamiento, Node vehiculo){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("vehiculo");
                relation.setTargetNode("rodamiento");
                relation.setDirectName("rodamientos");
                relation.setReverseName("vehiculo");
                relation.setSource(vehiculo);
                relation.setTarget(rodamiento);
                return relation;
        }

    public static Relation vehiculo_obligaciones(Node vehiculo,  Node obligaciones){
    //direct
            var relation = new Relation();
            relation.setSourceNode("vehiculo");
            relation.setTargetNode("obligacionesoperacion");
            relation.setDirectName("obligaciones");
            relation.setReverseName("vehiculo");
            relation.setSource(vehiculo);
            relation.setTarget(obligaciones);
            return relation;
    }

    public static Relation obligacionesoperacion_vehiculo(Node obligacionesoperacion, Node vehiculo){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("vehiculo");
                relation.setTargetNode("obligacionesoperacion");
                relation.setDirectName("obligaciones");
                relation.setReverseName("vehiculo");
                relation.setSource(vehiculo);
                relation.setTarget(obligacionesoperacion);
                return relation;
        }

    public static Relation vehiculo_turnos(Node vehiculo,  Node turnos){
    //direct
            var relation = new Relation();
            relation.setSourceNode("vehiculo");
            relation.setTargetNode("turno");
            relation.setDirectName("turnos");
            relation.setReverseName("vehiculo");
            relation.setSource(vehiculo);
            relation.setTarget(turnos);
            return relation;
    }

    public static Relation turno_vehiculo(Node turno, Node vehiculo){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("vehiculo");
                relation.setTargetNode("turno");
                relation.setDirectName("turnos");
                relation.setReverseName("vehiculo");
                relation.setSource(vehiculo);
                relation.setTarget(turno);
                return relation;
        }

    public static Relation vehiculo_posiciones(Node vehiculo,  Node posiciones){
    //direct
            var relation = new Relation();
            relation.setSourceNode("vehiculo");
            relation.setTargetNode("posicion");
            relation.setDirectName("posiciones");
            relation.setReverseName("vehiculo");
            relation.setSource(vehiculo);
            relation.setTarget(posiciones);
            return relation;
    }

    public static Relation posicion_vehiculo(Node posicion, Node vehiculo){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("vehiculo");
                relation.setTargetNode("posicion");
                relation.setDirectName("posiciones");
                relation.setReverseName("vehiculo");
                relation.setSource(vehiculo);
                relation.setTarget(posicion);
                return relation;
        }

    public static Relation vehiculo_incidentes(Node vehiculo,  Node incidentes){
    //direct
            var relation = new Relation();
            relation.setSourceNode("vehiculo");
            relation.setTargetNode("incidente");
            relation.setDirectName("incidentes");
            relation.setReverseName("vehiculo");
            relation.setSource(vehiculo);
            relation.setTarget(incidentes);
            return relation;
    }

    public static Relation incidente_vehiculo(Node incidente, Node vehiculo){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("vehiculo");
                relation.setTargetNode("incidente");
                relation.setDirectName("incidentes");
                relation.setReverseName("vehiculo");
                relation.setSource(vehiculo);
                relation.setTarget(incidente);
                return relation;
        }

    public static Relation vehiculo_conductores(Node vehiculo,  Node conductores){
    //direct
            var relation = new Relation();
            relation.setSourceNode("vehiculo");
            relation.setTargetNode("persona");
            relation.setDirectName("conductores");
            relation.setReverseName("maneja");
            relation.setSource(vehiculo);
            relation.setTarget(conductores);
            return relation;
    }

    public static Relation persona_maneja(Node persona, Node maneja){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("vehiculo");
                relation.setTargetNode("persona");
                relation.setDirectName("conductores");
                relation.setReverseName("maneja");
                relation.setSource(maneja);
                relation.setTarget(persona);
                return relation;
        }

    public static Relation zona_barrios(Node zona,  Node barrios){
    //direct
            var relation = new Relation();
            relation.setSourceNode("zona");
            relation.setTargetNode("barrio");
            relation.setDirectName("barrios");
            relation.setReverseName("tarifa");
            relation.setSource(zona);
            relation.setTarget(barrios);
            return relation;
    }

    public static Relation barrio_tarifa(Node barrio, Node tarifa){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("zona");
                relation.setTargetNode("barrio");
                relation.setDirectName("barrios");
                relation.setReverseName("tarifa");
                relation.setSource(tarifa);
                relation.setTarget(barrio);
                return relation;
        }

    public static Relation municipio_tarifas(Node municipio,  Node tarifas){
    //direct
            var relation = new Relation();
            relation.setSourceNode("municipio");
            relation.setTargetNode("tarifa");
            relation.setDirectName("tarifas");
            relation.setReverseName("municipio");
            relation.setSource(municipio);
            relation.setTarget(tarifas);
            return relation;
    }

    public static Relation tarifa_municipio(Node tarifa, Node municipio){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("municipio");
                relation.setTargetNode("tarifa");
                relation.setDirectName("tarifas");
                relation.setReverseName("municipio");
                relation.setSource(municipio);
                relation.setTarget(tarifa);
                return relation;
        }

    public static Relation municipio_empresas(Node municipio,  Node empresas){
    //direct
            var relation = new Relation();
            relation.setSourceNode("municipio");
            relation.setTargetNode("empresa");
            relation.setDirectName("empresas");
            relation.setReverseName("municipio");
            relation.setSource(municipio);
            relation.setTarget(empresas);
            return relation;
    }

    public static Relation empresa_municipio(Node empresa, Node municipio){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("municipio");
                relation.setTargetNode("empresa");
                relation.setDirectName("empresas");
                relation.setReverseName("municipio");
                relation.setSource(municipio);
                relation.setTarget(empresa);
                return relation;
        }

    public static Relation municipio_zonas(Node municipio,  Node zonas){
    //direct
            var relation = new Relation();
            relation.setSourceNode("municipio");
            relation.setTargetNode("zona");
            relation.setDirectName("zonas");
            relation.setReverseName("municipio");
            relation.setSource(municipio);
            relation.setTarget(zonas);
            return relation;
    }

    public static Relation zona_municipio(Node zona, Node municipio){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("municipio");
                relation.setTargetNode("zona");
                relation.setDirectName("zonas");
                relation.setReverseName("municipio");
                relation.setSource(municipio);
                relation.setTarget(zona);
                return relation;
        }

    public static Relation empresa_vehiculos(Node empresa,  Node vehiculos){
    //direct
            var relation = new Relation();
            relation.setSourceNode("empresa");
            relation.setTargetNode("vehiculo");
            relation.setDirectName("vehiculos");
            relation.setReverseName("empresa");
            relation.setSource(empresa);
            relation.setTarget(vehiculos);
            return relation;
    }

    public static Relation vehiculo_empresa(Node vehiculo, Node empresa){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("empresa");
                relation.setTargetNode("vehiculo");
                relation.setDirectName("vehiculos");
                relation.setReverseName("empresa");
                relation.setSource(empresa);
                relation.setTarget(vehiculo);
                return relation;
        }

    public static Relation empresa_persona(Node empresa,  Node persona){
    //direct
            var relation = new Relation();
            relation.setSourceNode("empresa");
            relation.setTargetNode("persona");
            relation.setDirectName("persona");
            relation.setReverseName("empresa");
            relation.setSource(empresa);
            relation.setTarget(persona);
            return relation;
    }

    public static Relation persona_empresa(Node persona, Node empresa){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("empresa");
                relation.setTargetNode("persona");
                relation.setDirectName("persona");
                relation.setReverseName("empresa");
                relation.setSource(empresa);
                relation.setTarget(persona);
                return relation;
        }

    public static Relation empresa_usuarios(Node empresa,  Node usuarios){
    //direct
            var relation = new Relation();
            relation.setSourceNode("empresa");
            relation.setTargetNode("usuariocompania");
            relation.setDirectName("usuarios");
            relation.setReverseName("compania");
            relation.setSource(empresa);
            relation.setTarget(usuarios);
            return relation;
    }

    public static Relation usuariocompania_compania(Node usuariocompania, Node compania){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("empresa");
                relation.setTargetNode("usuariocompania");
                relation.setDirectName("usuarios");
                relation.setReverseName("compania");
                relation.setSource(compania);
                relation.setTarget(usuariocompania);
                return relation;
        }

    public static Relation servicio_calificacion(Node servicio,  Node calificacion){
    //direct
            var relation = new Relation();
            relation.setSourceNode("servicio");
            relation.setTargetNode("calificacion");
            relation.setDirectName("calificacion");
            relation.setReverseName("servicio");
            relation.setSource(servicio);
            relation.setTarget(calificacion);
            return relation;
    }

    public static Relation calificacion_servicio(Node calificacion, Node servicio){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("servicio");
                relation.setTargetNode("calificacion");
                relation.setDirectName("calificacion");
                relation.setReverseName("servicio");
                relation.setSource(servicio);
                relation.setTarget(calificacion);
                return relation;
        }

    public static Relation servicio_conductor(Node servicio,  Node conductor){
    //direct
            var relation = new Relation();
            relation.setSourceNode("servicio");
            relation.setTargetNode("persona");
            relation.setDirectName("conductor");
            relation.setReverseName("servicio");
            relation.setSource(servicio);
            relation.setTarget(conductor);
            return relation;
    }

    public static Relation persona_servicio(Node persona, Node servicio){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("servicio");
                relation.setTargetNode("persona");
                relation.setDirectName("conductor");
                relation.setReverseName("servicio");
                relation.setSource(servicio);
                relation.setTarget(persona);
                return relation;
        }

    public static Relation usuario_persona(Node usuario,  Node persona){
    //direct
            var relation = new Relation();
            relation.setSourceNode("usuario");
            relation.setTargetNode("persona");
            relation.setDirectName("persona");
            relation.setReverseName("usuario");
            relation.setSource(usuario);
            relation.setTarget(persona);
            return relation;
    }

    public static Relation persona_usuario(Node persona, Node usuario){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("usuario");
                relation.setTargetNode("persona");
                relation.setDirectName("persona");
                relation.setReverseName("usuario");
                relation.setSource(usuario);
                relation.setTarget(persona);
                return relation;
        }

    public static Relation usuario_companias(Node usuario,  Node companias){
    //direct
            var relation = new Relation();
            relation.setSourceNode("usuario");
            relation.setTargetNode("usuariocompania");
            relation.setDirectName("companias");
            relation.setReverseName("usuario");
            relation.setSource(usuario);
            relation.setTarget(companias);
            return relation;
    }

    public static Relation usuariocompania_usuario(Node usuariocompania, Node usuario){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("usuario");
                relation.setTargetNode("usuariocompania");
                relation.setDirectName("companias");
                relation.setReverseName("usuario");
                relation.setSource(usuario);
                relation.setTarget(usuariocompania);
                return relation;
        }

    public static Relation servicio_vehiculosservicio(Node servicio,  Node vehiculosservicio){
    //direct
            var relation = new Relation();
            relation.setSourceNode("servicio");
            relation.setTargetNode("vehiculoservicio");
            relation.setDirectName("vehiculosservicio");
            relation.setReverseName("servicio");
            relation.setSource(servicio);
            relation.setTarget(vehiculosservicio);
            return relation;
    }

    public static Relation vehiculoservicio_servicio(Node vehiculoservicio, Node servicio){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("servicio");
                relation.setTargetNode("vehiculoservicio");
                relation.setDirectName("vehiculosservicio");
                relation.setReverseName("servicio");
                relation.setSource(servicio);
                relation.setTarget(vehiculoservicio);
                return relation;
        }

    public static Relation vehiculo_vehiculosservicio(Node vehiculo,  Node vehiculosservicio){
    //direct
            var relation = new Relation();
            relation.setSourceNode("vehiculo");
            relation.setTargetNode("vehiculoservicio");
            relation.setDirectName("vehiculosservicio");
            relation.setReverseName("vehiculo");
            relation.setSource(vehiculo);
            relation.setTarget(vehiculosservicio);
            return relation;
    }

    public static Relation vehiculoservicio_vehiculo(Node vehiculoservicio, Node vehiculo){
    //reverse
    var relation = new Relation();
                relation.setSourceNode("vehiculo");
                relation.setTargetNode("vehiculoservicio");
                relation.setDirectName("vehiculosservicio");
                relation.setReverseName("vehiculo");
                relation.setSource(vehiculo);
                relation.setTarget(vehiculoservicio);
                return relation;
        }



}
