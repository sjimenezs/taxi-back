package bee.taxiservices.services.base ;

import bee.persistence.GraphQLTypes;
import bee.datastorage.graphql.resolver.ListOperationResolver;
import bee.datastorage.graphql.DataStorageExecutor;
import bee.datastorage.graphql.DataStorageOperation;
import bee.datastorage.graphql.resolver.OperationResolver;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.session.ExecutionContext;
import graphql.schema.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import bee.datastorage.model.EStorageStatus;
import java.util.function.BiFunction;

import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;

public abstract class identificacion_consultar_por_idBase extends DataStorageExecutor< Node > {
    @Override
    public  Node  execute(DataFetchingEnvironment dataFetchingEnvironment, ExecutionContext executionContext)  throws BusinessException {

        EStorageStatus status = null;


UUID input = (UUID)dataFetchingEnvironment.getArgument("input");
Node dataResponse = this.execute( input, executionContext);

    if (dataResponse == null){
        return null;
    }
        return dataResponse;
    }

    public abstract Node execute(UUID input, ExecutionContext executionContext)  throws BusinessException;

    @Override
    public GraphQLFieldDefinition buildOperationType(DataStorageOperation operation) {
        GraphQLFieldDefinition.Builder type = newFieldDefinition()
                .name(operation.getName());

        type.argument(GraphQLArgument.newArgument().name("input").type(GraphQLTypes.GraphQLID).build());

        type.type(GraphQLTypeReference.typeRef("identificacion"));
        type.dataFetcher(OperationResolver.INSTANCE);

        return type.build();
    }
}
