package bee.taxiservices.services.base ;

import bee.datastorage.model.EStorageStatus;
import bee.datastorage.model.storage.NodeProperty;
import bee.datastorage.model.storage.Node;
import bee.datastorage.model.storage.Relation;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.List;

public class Mappers {
    public static Map<String, BiFunction<Map,EStorageStatus, Object>> mappers = new HashMap<>();

    static {

        mappers.put("persona_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("persona");

            if (in.containsKey("nombre")){
              NodeProperty nodePropertynombre = new NodeProperty();
              nodePropertynombre.setPropertyName("nombre");
              nodePropertynombre.setValue(in.get("nombre"));
              out.addProperty(nodePropertynombre);
            }

            if (in.containsKey("tipopersona")){
              NodeProperty nodePropertytipopersona = new NodeProperty();
              nodePropertytipopersona.setPropertyName("tipopersona");
              nodePropertytipopersona.setValue(in.get("tipopersona"));
              out.addProperty(nodePropertytipopersona);
            }

            if (mappers.get("identificacion_mapper")!=null && in.get("identificacion") != null){
                //direct
                var relationidentificacion = new Relation();
                relationidentificacion.setSourceNode("persona");
                relationidentificacion.setTargetNode("identificacion");
                relationidentificacion.setDirectName("identificacion");
                relationidentificacion.setReverseName("persona");
                relationidentificacion.setSource(out);
                if (in.get("identificacion") instanceof Map){
                  relationidentificacion.setTarget((Node)mappers.get("identificacion_mapper").apply((Map)in.get("identificacion"), status));
                }else if (in.get("identificacion") instanceof List) {
                  relationidentificacion.setTarget((Node)mappers.get("identificacion_mapper").apply((Map)((List)in.get("identificacion")).get(0), status));
                }

                out.addRelation(relationidentificacion);
            }



            if (mappers.get("contacto_mapper")!=null && in.get("contacto") != null){
                //direct
                var relationcontacto = new Relation();
                relationcontacto.setSourceNode("persona");
                relationcontacto.setTargetNode("contacto");
                relationcontacto.setDirectName("contacto");
                relationcontacto.setReverseName("persona");
                relationcontacto.setSource(out);
                if (in.get("contacto") instanceof Map){
                  relationcontacto.setTarget((Node)mappers.get("contacto_mapper").apply((Map)in.get("contacto"), status));
                }else if (in.get("contacto") instanceof List) {
                  relationcontacto.setTarget((Node)mappers.get("contacto_mapper").apply((Map)((List)in.get("contacto")).get(0), status));
                }

                out.addRelation(relationcontacto);
            }



            if (mappers.get("personanatural_mapper")!=null && in.get("persona_natural") != null){
                //direct
                var relationpersona_natural = new Relation();
                relationpersona_natural.setSourceNode("persona");
                relationpersona_natural.setTargetNode("personanatural");
                relationpersona_natural.setDirectName("persona_natural");
                relationpersona_natural.setReverseName("persona");
                relationpersona_natural.setSource(out);
                if (in.get("persona_natural") instanceof Map){
                  relationpersona_natural.setTarget((Node)mappers.get("personanatural_mapper").apply((Map)in.get("persona_natural"), status));
                }else if (in.get("persona_natural") instanceof List) {
                  relationpersona_natural.setTarget((Node)mappers.get("personanatural_mapper").apply((Map)((List)in.get("persona_natural")).get(0), status));
                }

                out.addRelation(relationpersona_natural);
            }



            if (mappers.get("vehiculo_mapper")!=null && in.get("vehiculo") != null){
                //direct
                var relationvehiculo = new Relation();
                relationvehiculo.setSourceNode("persona");
                relationvehiculo.setTargetNode("vehiculo");
                relationvehiculo.setDirectName("vehiculo");
                relationvehiculo.setReverseName("dueno");
                relationvehiculo.setSource(out);
                if (in.get("vehiculo") instanceof Map){
                  relationvehiculo.setTarget((Node)mappers.get("vehiculo_mapper").apply((Map)in.get("vehiculo"), status));
                }else if (in.get("vehiculo") instanceof List) {
                  relationvehiculo.setTarget((Node)mappers.get("vehiculo_mapper").apply((Map)((List)in.get("vehiculo")).get(0), status));
                }

                out.addRelation(relationvehiculo);
            }



            if (mappers.get("servicio_mapper")!=null && in.get("servicios") != null){
                //direct
                var relationservicios = new Relation();
                relationservicios.setSourceNode("persona");
                relationservicios.setTargetNode("servicio");
                relationservicios.setDirectName("servicios");
                relationservicios.setReverseName("persona");
                relationservicios.setSource(out);
                if (in.get("servicios") instanceof Map){
                  relationservicios.setTarget((Node)mappers.get("servicio_mapper").apply((Map)in.get("servicios"), status));
                }else if (in.get("servicios") instanceof List) {
                  relationservicios.setTarget((Node)mappers.get("servicio_mapper").apply((Map)((List)in.get("servicios")).get(0), status));
                }

                out.addRelation(relationservicios);
            }



            if (mappers.get("turno_mapper")!=null && in.get("turnos") != null){
                //direct
                var relationturnos = new Relation();
                relationturnos.setSourceNode("persona");
                relationturnos.setTargetNode("turno");
                relationturnos.setDirectName("turnos");
                relationturnos.setReverseName("conductor");
                relationturnos.setSource(out);
                if (in.get("turnos") instanceof Map){
                  relationturnos.setTarget((Node)mappers.get("turno_mapper").apply((Map)in.get("turnos"), status));
                }else if (in.get("turnos") instanceof List) {
                  relationturnos.setTarget((Node)mappers.get("turno_mapper").apply((Map)((List)in.get("turnos")).get(0), status));
                }

                out.addRelation(relationturnos);
            }




            if (mappers.get("vehiculo_mapper")!=null && in.get("maneja") != null){
                //reverse
                var relationmaneja = new Relation();
                relationmaneja.setSourceNode("vehiculo");
                relationmaneja.setTargetNode("persona");
                relationmaneja.setDirectName("conductores");
                relationmaneja.setReverseName("maneja");
                relationmaneja.setTarget(out);
                if (in.get("maneja") instanceof Map){
                  relationmaneja.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)in.get("maneja"), status));
                }else if (in.get("maneja") instanceof List) {
                  relationmaneja.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)((List)in.get("maneja")).get(0), status));
                }

                out.addRelation(relationmaneja);
            }



            if (mappers.get("empresa_mapper")!=null && in.get("empresa") != null){
                //reverse
                var relationempresa = new Relation();
                relationempresa.setSourceNode("empresa");
                relationempresa.setTargetNode("persona");
                relationempresa.setDirectName("persona");
                relationempresa.setReverseName("empresa");
                relationempresa.setTarget(out);
                if (in.get("empresa") instanceof Map){
                  relationempresa.setSource((Node)mappers.get("empresa_mapper").apply((Map)in.get("empresa"), status));
                }else if (in.get("empresa") instanceof List) {
                  relationempresa.setSource((Node)mappers.get("empresa_mapper").apply((Map)((List)in.get("empresa")).get(0), status));
                }

                out.addRelation(relationempresa);
            }



            if (mappers.get("servicio_mapper")!=null && in.get("servicio") != null){
                //reverse
                var relationservicio = new Relation();
                relationservicio.setSourceNode("servicio");
                relationservicio.setTargetNode("persona");
                relationservicio.setDirectName("conductor");
                relationservicio.setReverseName("servicio");
                relationservicio.setTarget(out);
                if (in.get("servicio") instanceof Map){
                  relationservicio.setSource((Node)mappers.get("servicio_mapper").apply((Map)in.get("servicio"), status));
                }else if (in.get("servicio") instanceof List) {
                  relationservicio.setSource((Node)mappers.get("servicio_mapper").apply((Map)((List)in.get("servicio")).get(0), status));
                }

                out.addRelation(relationservicio);
            }



            if (mappers.get("usuario_mapper")!=null && in.get("usuario") != null){
                //reverse
                var relationusuario = new Relation();
                relationusuario.setSourceNode("usuario");
                relationusuario.setTargetNode("persona");
                relationusuario.setDirectName("persona");
                relationusuario.setReverseName("usuario");
                relationusuario.setTarget(out);
                if (in.get("usuario") instanceof Map){
                  relationusuario.setSource((Node)mappers.get("usuario_mapper").apply((Map)in.get("usuario"), status));
                }else if (in.get("usuario") instanceof List) {
                  relationusuario.setSource((Node)mappers.get("usuario_mapper").apply((Map)((List)in.get("usuario")).get(0), status));
                }

                out.addRelation(relationusuario);
            }


            return out;
        });
        mappers.put("identificacion_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("identificacion");

            if (in.containsKey("tipoidentificacion")){
              NodeProperty nodePropertytipoidentificacion = new NodeProperty();
              nodePropertytipoidentificacion.setPropertyName("tipoidentificacion");
              nodePropertytipoidentificacion.setValue(in.get("tipoidentificacion"));
              out.addProperty(nodePropertytipoidentificacion);
            }

            if (in.containsKey("identificacion")){
              NodeProperty nodePropertyidentificacion = new NodeProperty();
              nodePropertyidentificacion.setPropertyName("identificacion");
              nodePropertyidentificacion.setValue(in.get("identificacion"));
              out.addProperty(nodePropertyidentificacion);
            }


            if (mappers.get("persona_mapper")!=null && in.get("persona") != null){
                //reverse
                var relationpersona = new Relation();
                relationpersona.setSourceNode("persona");
                relationpersona.setTargetNode("identificacion");
                relationpersona.setDirectName("identificacion");
                relationpersona.setReverseName("persona");
                relationpersona.setTarget(out);
                if (in.get("persona") instanceof Map){
                  relationpersona.setSource((Node)mappers.get("persona_mapper").apply((Map)in.get("persona"), status));
                }else if (in.get("persona") instanceof List) {
                  relationpersona.setSource((Node)mappers.get("persona_mapper").apply((Map)((List)in.get("persona")).get(0), status));
                }

                out.addRelation(relationpersona);
            }


            return out;
        });
        mappers.put("contacto_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("contacto");

            if (in.containsKey("email")){
              NodeProperty nodePropertyemail = new NodeProperty();
              nodePropertyemail.setPropertyName("email");
              nodePropertyemail.setValue(in.get("email"));
              out.addProperty(nodePropertyemail);
            }

            if (in.containsKey("celular")){
              NodeProperty nodePropertycelular = new NodeProperty();
              nodePropertycelular.setPropertyName("celular");
              nodePropertycelular.setValue(in.get("celular"));
              out.addProperty(nodePropertycelular);
            }


            if (mappers.get("persona_mapper")!=null && in.get("persona") != null){
                //reverse
                var relationpersona = new Relation();
                relationpersona.setSourceNode("persona");
                relationpersona.setTargetNode("contacto");
                relationpersona.setDirectName("contacto");
                relationpersona.setReverseName("persona");
                relationpersona.setTarget(out);
                if (in.get("persona") instanceof Map){
                  relationpersona.setSource((Node)mappers.get("persona_mapper").apply((Map)in.get("persona"), status));
                }else if (in.get("persona") instanceof List) {
                  relationpersona.setSource((Node)mappers.get("persona_mapper").apply((Map)((List)in.get("persona")).get(0), status));
                }

                out.addRelation(relationpersona);
            }


            return out;
        });
        mappers.put("vehiculo_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("vehiculo");

            if (in.containsKey("placa")){
              NodeProperty nodePropertyplaca = new NodeProperty();
              nodePropertyplaca.setPropertyName("placa");
              nodePropertyplaca.setValue(in.get("placa"));
              out.addProperty(nodePropertyplaca);
            }

            if (in.containsKey("estado")){
              NodeProperty nodePropertyestado = new NodeProperty();
              nodePropertyestado.setPropertyName("estado");
              nodePropertyestado.setValue(in.get("estado"));
              out.addProperty(nodePropertyestado);
            }

            if (in.containsKey("numero")){
              NodeProperty nodePropertynumero = new NodeProperty();
              nodePropertynumero.setPropertyName("numero");
              nodePropertynumero.setValue(in.get("numero"));
              out.addProperty(nodePropertynumero);
            }

            if (in.containsKey("disponibilidad")){
              NodeProperty nodePropertydisponibilidad = new NodeProperty();
              nodePropertydisponibilidad.setPropertyName("disponibilidad");
              nodePropertydisponibilidad.setValue(in.get("disponibilidad"));
              out.addProperty(nodePropertydisponibilidad);
            }

            if (in.containsKey("idpush")){
              NodeProperty nodePropertyidpush = new NodeProperty();
              nodePropertyidpush.setPropertyName("idpush");
              nodePropertyidpush.setValue(in.get("idpush"));
              out.addProperty(nodePropertyidpush);
            }

            if (mappers.get("servicio_mapper")!=null && in.get("serviciosvehiculo") != null){
                //direct
                var relationserviciosvehiculo = new Relation();
                relationserviciosvehiculo.setSourceNode("vehiculo");
                relationserviciosvehiculo.setTargetNode("servicio");
                relationserviciosvehiculo.setDirectName("serviciosvehiculo");
                relationserviciosvehiculo.setReverseName("vehiculo");
                relationserviciosvehiculo.setSource(out);
                if (in.get("serviciosvehiculo") instanceof Map){
                  relationserviciosvehiculo.setTarget((Node)mappers.get("servicio_mapper").apply((Map)in.get("serviciosvehiculo"), status));
                }else if (in.get("serviciosvehiculo") instanceof List) {
                  relationserviciosvehiculo.setTarget((Node)mappers.get("servicio_mapper").apply((Map)((List)in.get("serviciosvehiculo")).get(0), status));
                }

                out.addRelation(relationserviciosvehiculo);
            }



            if (mappers.get("rodamiento_mapper")!=null && in.get("rodamientos") != null){
                //direct
                var relationrodamientos = new Relation();
                relationrodamientos.setSourceNode("vehiculo");
                relationrodamientos.setTargetNode("rodamiento");
                relationrodamientos.setDirectName("rodamientos");
                relationrodamientos.setReverseName("vehiculo");
                relationrodamientos.setSource(out);
                if (in.get("rodamientos") instanceof Map){
                  relationrodamientos.setTarget((Node)mappers.get("rodamiento_mapper").apply((Map)in.get("rodamientos"), status));
                }else if (in.get("rodamientos") instanceof List) {
                  relationrodamientos.setTarget((Node)mappers.get("rodamiento_mapper").apply((Map)((List)in.get("rodamientos")).get(0), status));
                }

                out.addRelation(relationrodamientos);
            }



            if (mappers.get("obligacionesoperacion_mapper")!=null && in.get("obligaciones") != null){
                //direct
                var relationobligaciones = new Relation();
                relationobligaciones.setSourceNode("vehiculo");
                relationobligaciones.setTargetNode("obligacionesoperacion");
                relationobligaciones.setDirectName("obligaciones");
                relationobligaciones.setReverseName("vehiculo");
                relationobligaciones.setSource(out);
                if (in.get("obligaciones") instanceof Map){
                  relationobligaciones.setTarget((Node)mappers.get("obligacionesoperacion_mapper").apply((Map)in.get("obligaciones"), status));
                }else if (in.get("obligaciones") instanceof List) {
                  relationobligaciones.setTarget((Node)mappers.get("obligacionesoperacion_mapper").apply((Map)((List)in.get("obligaciones")).get(0), status));
                }

                out.addRelation(relationobligaciones);
            }



            if (mappers.get("turno_mapper")!=null && in.get("turnos") != null){
                //direct
                var relationturnos = new Relation();
                relationturnos.setSourceNode("vehiculo");
                relationturnos.setTargetNode("turno");
                relationturnos.setDirectName("turnos");
                relationturnos.setReverseName("vehiculo");
                relationturnos.setSource(out);
                if (in.get("turnos") instanceof Map){
                  relationturnos.setTarget((Node)mappers.get("turno_mapper").apply((Map)in.get("turnos"), status));
                }else if (in.get("turnos") instanceof List) {
                  relationturnos.setTarget((Node)mappers.get("turno_mapper").apply((Map)((List)in.get("turnos")).get(0), status));
                }

                out.addRelation(relationturnos);
            }



            if (mappers.get("posicion_mapper")!=null && in.get("posiciones") != null){
                //direct
                var relationposiciones = new Relation();
                relationposiciones.setSourceNode("vehiculo");
                relationposiciones.setTargetNode("posicion");
                relationposiciones.setDirectName("posiciones");
                relationposiciones.setReverseName("vehiculo");
                relationposiciones.setSource(out);
                if (in.get("posiciones") instanceof Map){
                  relationposiciones.setTarget((Node)mappers.get("posicion_mapper").apply((Map)in.get("posiciones"), status));
                }else if (in.get("posiciones") instanceof List) {
                  relationposiciones.setTarget((Node)mappers.get("posicion_mapper").apply((Map)((List)in.get("posiciones")).get(0), status));
                }

                out.addRelation(relationposiciones);
            }



            if (mappers.get("incidente_mapper")!=null && in.get("incidentes") != null){
                //direct
                var relationincidentes = new Relation();
                relationincidentes.setSourceNode("vehiculo");
                relationincidentes.setTargetNode("incidente");
                relationincidentes.setDirectName("incidentes");
                relationincidentes.setReverseName("vehiculo");
                relationincidentes.setSource(out);
                if (in.get("incidentes") instanceof Map){
                  relationincidentes.setTarget((Node)mappers.get("incidente_mapper").apply((Map)in.get("incidentes"), status));
                }else if (in.get("incidentes") instanceof List) {
                  relationincidentes.setTarget((Node)mappers.get("incidente_mapper").apply((Map)((List)in.get("incidentes")).get(0), status));
                }

                out.addRelation(relationincidentes);
            }



            if (mappers.get("persona_mapper")!=null && in.get("conductores") != null){
                //direct
                var relationconductores = new Relation();
                relationconductores.setSourceNode("vehiculo");
                relationconductores.setTargetNode("persona");
                relationconductores.setDirectName("conductores");
                relationconductores.setReverseName("maneja");
                relationconductores.setSource(out);
                if (in.get("conductores") instanceof Map){
                  relationconductores.setTarget((Node)mappers.get("persona_mapper").apply((Map)in.get("conductores"), status));
                }else if (in.get("conductores") instanceof List) {
                  relationconductores.setTarget((Node)mappers.get("persona_mapper").apply((Map)((List)in.get("conductores")).get(0), status));
                }

                out.addRelation(relationconductores);
            }



            if (mappers.get("vehiculoservicio_mapper")!=null && in.get("vehiculosservicio") != null){
                //direct
                var relationvehiculosservicio = new Relation();
                relationvehiculosservicio.setSourceNode("vehiculo");
                relationvehiculosservicio.setTargetNode("vehiculoservicio");
                relationvehiculosservicio.setDirectName("vehiculosservicio");
                relationvehiculosservicio.setReverseName("vehiculo");
                relationvehiculosservicio.setSource(out);
                if (in.get("vehiculosservicio") instanceof Map){
                  relationvehiculosservicio.setTarget((Node)mappers.get("vehiculoservicio_mapper").apply((Map)in.get("vehiculosservicio"), status));
                }else if (in.get("vehiculosservicio") instanceof List) {
                  relationvehiculosservicio.setTarget((Node)mappers.get("vehiculoservicio_mapper").apply((Map)((List)in.get("vehiculosservicio")).get(0), status));
                }

                out.addRelation(relationvehiculosservicio);
            }




            if (mappers.get("persona_mapper")!=null && in.get("dueno") != null){
                //reverse
                var relationdueno = new Relation();
                relationdueno.setSourceNode("persona");
                relationdueno.setTargetNode("vehiculo");
                relationdueno.setDirectName("vehiculo");
                relationdueno.setReverseName("dueno");
                relationdueno.setTarget(out);
                if (in.get("dueno") instanceof Map){
                  relationdueno.setSource((Node)mappers.get("persona_mapper").apply((Map)in.get("dueno"), status));
                }else if (in.get("dueno") instanceof List) {
                  relationdueno.setSource((Node)mappers.get("persona_mapper").apply((Map)((List)in.get("dueno")).get(0), status));
                }

                out.addRelation(relationdueno);
            }



            if (mappers.get("empresa_mapper")!=null && in.get("empresa") != null){
                //reverse
                var relationempresa = new Relation();
                relationempresa.setSourceNode("empresa");
                relationempresa.setTargetNode("vehiculo");
                relationempresa.setDirectName("vehiculos");
                relationempresa.setReverseName("empresa");
                relationempresa.setTarget(out);
                if (in.get("empresa") instanceof Map){
                  relationempresa.setSource((Node)mappers.get("empresa_mapper").apply((Map)in.get("empresa"), status));
                }else if (in.get("empresa") instanceof List) {
                  relationempresa.setSource((Node)mappers.get("empresa_mapper").apply((Map)((List)in.get("empresa")).get(0), status));
                }

                out.addRelation(relationempresa);
            }


            return out;
        });
        mappers.put("tarifa_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("tarifa");

            if (in.containsKey("serviciohora")){
              NodeProperty nodePropertyserviciohora = new NodeProperty();
              nodePropertyserviciohora.setPropertyName("serviciohora");
              nodePropertyserviciohora.setValue(in.get("serviciohora"));
              out.addProperty(nodePropertyserviciohora);
            }

            if (in.containsKey("primaespecial")){
              NodeProperty nodePropertyprimaespecial = new NodeProperty();
              nodePropertyprimaespecial.setPropertyName("primaespecial");
              nodePropertyprimaespecial.setValue(in.get("primaespecial"));
              out.addProperty(nodePropertyprimaespecial);
            }


            if (mappers.get("municipio_mapper")!=null && in.get("municipio") != null){
                //reverse
                var relationmunicipio = new Relation();
                relationmunicipio.setSourceNode("municipio");
                relationmunicipio.setTargetNode("tarifa");
                relationmunicipio.setDirectName("tarifas");
                relationmunicipio.setReverseName("municipio");
                relationmunicipio.setTarget(out);
                if (in.get("municipio") instanceof Map){
                  relationmunicipio.setSource((Node)mappers.get("municipio_mapper").apply((Map)in.get("municipio"), status));
                }else if (in.get("municipio") instanceof List) {
                  relationmunicipio.setSource((Node)mappers.get("municipio_mapper").apply((Map)((List)in.get("municipio")).get(0), status));
                }

                out.addRelation(relationmunicipio);
            }


            return out;
        });
        mappers.put("municipio_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("municipio");

            if (in.containsKey("codigo")){
              NodeProperty nodePropertycodigo = new NodeProperty();
              nodePropertycodigo.setPropertyName("codigo");
              nodePropertycodigo.setValue(in.get("codigo"));
              out.addProperty(nodePropertycodigo);
            }

            if (in.containsKey("nombre")){
              NodeProperty nodePropertynombre = new NodeProperty();
              nodePropertynombre.setPropertyName("nombre");
              nodePropertynombre.setValue(in.get("nombre"));
              out.addProperty(nodePropertynombre);
            }

            if (mappers.get("tarifa_mapper")!=null && in.get("tarifas") != null){
                //direct
                var relationtarifas = new Relation();
                relationtarifas.setSourceNode("municipio");
                relationtarifas.setTargetNode("tarifa");
                relationtarifas.setDirectName("tarifas");
                relationtarifas.setReverseName("municipio");
                relationtarifas.setSource(out);
                if (in.get("tarifas") instanceof Map){
                  relationtarifas.setTarget((Node)mappers.get("tarifa_mapper").apply((Map)in.get("tarifas"), status));
                }else if (in.get("tarifas") instanceof List) {
                  relationtarifas.setTarget((Node)mappers.get("tarifa_mapper").apply((Map)((List)in.get("tarifas")).get(0), status));
                }

                out.addRelation(relationtarifas);
            }



            if (mappers.get("empresa_mapper")!=null && in.get("empresas") != null){
                //direct
                var relationempresas = new Relation();
                relationempresas.setSourceNode("municipio");
                relationempresas.setTargetNode("empresa");
                relationempresas.setDirectName("empresas");
                relationempresas.setReverseName("municipio");
                relationempresas.setSource(out);
                if (in.get("empresas") instanceof Map){
                  relationempresas.setTarget((Node)mappers.get("empresa_mapper").apply((Map)in.get("empresas"), status));
                }else if (in.get("empresas") instanceof List) {
                  relationempresas.setTarget((Node)mappers.get("empresa_mapper").apply((Map)((List)in.get("empresas")).get(0), status));
                }

                out.addRelation(relationempresas);
            }



            if (mappers.get("zona_mapper")!=null && in.get("zonas") != null){
                //direct
                var relationzonas = new Relation();
                relationzonas.setSourceNode("municipio");
                relationzonas.setTargetNode("zona");
                relationzonas.setDirectName("zonas");
                relationzonas.setReverseName("municipio");
                relationzonas.setSource(out);
                if (in.get("zonas") instanceof Map){
                  relationzonas.setTarget((Node)mappers.get("zona_mapper").apply((Map)in.get("zonas"), status));
                }else if (in.get("zonas") instanceof List) {
                  relationzonas.setTarget((Node)mappers.get("zona_mapper").apply((Map)((List)in.get("zonas")).get(0), status));
                }

                out.addRelation(relationzonas);
            }



            return out;
        });
        mappers.put("empresa_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("empresa");

            if (in.containsKey("estado")){
              NodeProperty nodePropertyestado = new NodeProperty();
              nodePropertyestado.setPropertyName("estado");
              nodePropertyestado.setValue(in.get("estado"));
              out.addProperty(nodePropertyestado);
            }

            if (mappers.get("vehiculo_mapper")!=null && in.get("vehiculos") != null){
                //direct
                var relationvehiculos = new Relation();
                relationvehiculos.setSourceNode("empresa");
                relationvehiculos.setTargetNode("vehiculo");
                relationvehiculos.setDirectName("vehiculos");
                relationvehiculos.setReverseName("empresa");
                relationvehiculos.setSource(out);
                if (in.get("vehiculos") instanceof Map){
                  relationvehiculos.setTarget((Node)mappers.get("vehiculo_mapper").apply((Map)in.get("vehiculos"), status));
                }else if (in.get("vehiculos") instanceof List) {
                  relationvehiculos.setTarget((Node)mappers.get("vehiculo_mapper").apply((Map)((List)in.get("vehiculos")).get(0), status));
                }

                out.addRelation(relationvehiculos);
            }



            if (mappers.get("persona_mapper")!=null && in.get("persona") != null){
                //direct
                var relationpersona = new Relation();
                relationpersona.setSourceNode("empresa");
                relationpersona.setTargetNode("persona");
                relationpersona.setDirectName("persona");
                relationpersona.setReverseName("empresa");
                relationpersona.setSource(out);
                if (in.get("persona") instanceof Map){
                  relationpersona.setTarget((Node)mappers.get("persona_mapper").apply((Map)in.get("persona"), status));
                }else if (in.get("persona") instanceof List) {
                  relationpersona.setTarget((Node)mappers.get("persona_mapper").apply((Map)((List)in.get("persona")).get(0), status));
                }

                out.addRelation(relationpersona);
            }



            if (mappers.get("usuariocompania_mapper")!=null && in.get("usuarios") != null){
                //direct
                var relationusuarios = new Relation();
                relationusuarios.setSourceNode("empresa");
                relationusuarios.setTargetNode("usuariocompania");
                relationusuarios.setDirectName("usuarios");
                relationusuarios.setReverseName("compania");
                relationusuarios.setSource(out);
                if (in.get("usuarios") instanceof Map){
                  relationusuarios.setTarget((Node)mappers.get("usuariocompania_mapper").apply((Map)in.get("usuarios"), status));
                }else if (in.get("usuarios") instanceof List) {
                  relationusuarios.setTarget((Node)mappers.get("usuariocompania_mapper").apply((Map)((List)in.get("usuarios")).get(0), status));
                }

                out.addRelation(relationusuarios);
            }




            if (mappers.get("municipio_mapper")!=null && in.get("municipio") != null){
                //reverse
                var relationmunicipio = new Relation();
                relationmunicipio.setSourceNode("municipio");
                relationmunicipio.setTargetNode("empresa");
                relationmunicipio.setDirectName("empresas");
                relationmunicipio.setReverseName("municipio");
                relationmunicipio.setTarget(out);
                if (in.get("municipio") instanceof Map){
                  relationmunicipio.setSource((Node)mappers.get("municipio_mapper").apply((Map)in.get("municipio"), status));
                }else if (in.get("municipio") instanceof List) {
                  relationmunicipio.setSource((Node)mappers.get("municipio_mapper").apply((Map)((List)in.get("municipio")).get(0), status));
                }

                out.addRelation(relationmunicipio);
            }


            return out;
        });
        mappers.put("personanatural_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("personanatural");

            if (in.containsKey("segundonombre")){
              NodeProperty nodePropertysegundonombre = new NodeProperty();
              nodePropertysegundonombre.setPropertyName("segundonombre");
              nodePropertysegundonombre.setValue(in.get("segundonombre"));
              out.addProperty(nodePropertysegundonombre);
            }

            if (in.containsKey("apellido")){
              NodeProperty nodePropertyapellido = new NodeProperty();
              nodePropertyapellido.setPropertyName("apellido");
              nodePropertyapellido.setValue(in.get("apellido"));
              out.addProperty(nodePropertyapellido);
            }

            if (in.containsKey("segundoapellido")){
              NodeProperty nodePropertysegundoapellido = new NodeProperty();
              nodePropertysegundoapellido.setPropertyName("segundoapellido");
              nodePropertysegundoapellido.setValue(in.get("segundoapellido"));
              out.addProperty(nodePropertysegundoapellido);
            }

            if (in.containsKey("foto")){
              NodeProperty nodePropertyfoto = new NodeProperty();
              nodePropertyfoto.setPropertyName("foto");
              nodePropertyfoto.setValue(in.get("foto"));
              out.addProperty(nodePropertyfoto);
            }


            if (mappers.get("persona_mapper")!=null && in.get("persona") != null){
                //reverse
                var relationpersona = new Relation();
                relationpersona.setSourceNode("persona");
                relationpersona.setTargetNode("personanatural");
                relationpersona.setDirectName("persona_natural");
                relationpersona.setReverseName("persona");
                relationpersona.setTarget(out);
                if (in.get("persona") instanceof Map){
                  relationpersona.setSource((Node)mappers.get("persona_mapper").apply((Map)in.get("persona"), status));
                }else if (in.get("persona") instanceof List) {
                  relationpersona.setSource((Node)mappers.get("persona_mapper").apply((Map)((List)in.get("persona")).get(0), status));
                }

                out.addRelation(relationpersona);
            }


            return out;
        });
        mappers.put("servicio_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("servicio");

            if (in.containsKey("longitudorigen")){
              NodeProperty nodePropertylongitudorigen = new NodeProperty();
              nodePropertylongitudorigen.setPropertyName("longitudorigen");
              nodePropertylongitudorigen.setValue(in.get("longitudorigen"));
              out.addProperty(nodePropertylongitudorigen);
            }

            if (in.containsKey("latitudorigen")){
              NodeProperty nodePropertylatitudorigen = new NodeProperty();
              nodePropertylatitudorigen.setPropertyName("latitudorigen");
              nodePropertylatitudorigen.setValue(in.get("latitudorigen"));
              out.addProperty(nodePropertylatitudorigen);
            }

            if (in.containsKey("longituddestino")){
              NodeProperty nodePropertylongituddestino = new NodeProperty();
              nodePropertylongituddestino.setPropertyName("longituddestino");
              nodePropertylongituddestino.setValue(in.get("longituddestino"));
              out.addProperty(nodePropertylongituddestino);
            }

            if (in.containsKey("latituddestino")){
              NodeProperty nodePropertylatituddestino = new NodeProperty();
              nodePropertylatituddestino.setPropertyName("latituddestino");
              nodePropertylatituddestino.setValue(in.get("latituddestino"));
              out.addProperty(nodePropertylatituddestino);
            }

            if (in.containsKey("direccionorigen")){
              NodeProperty nodePropertydireccionorigen = new NodeProperty();
              nodePropertydireccionorigen.setPropertyName("direccionorigen");
              nodePropertydireccionorigen.setValue(in.get("direccionorigen"));
              out.addProperty(nodePropertydireccionorigen);
            }

            if (in.containsKey("estado")){
              NodeProperty nodePropertyestado = new NodeProperty();
              nodePropertyestado.setPropertyName("estado");
              nodePropertyestado.setValue(in.get("estado"));
              out.addProperty(nodePropertyestado);
            }

            if (in.containsKey("fecha")){
              NodeProperty nodePropertyfecha = new NodeProperty();
              nodePropertyfecha.setPropertyName("fecha");
              nodePropertyfecha.setValue(in.get("fecha"));
              out.addProperty(nodePropertyfecha);
            }

            if (mappers.get("calificacion_mapper")!=null && in.get("calificacion") != null){
                //direct
                var relationcalificacion = new Relation();
                relationcalificacion.setSourceNode("servicio");
                relationcalificacion.setTargetNode("calificacion");
                relationcalificacion.setDirectName("calificacion");
                relationcalificacion.setReverseName("servicio");
                relationcalificacion.setSource(out);
                if (in.get("calificacion") instanceof Map){
                  relationcalificacion.setTarget((Node)mappers.get("calificacion_mapper").apply((Map)in.get("calificacion"), status));
                }else if (in.get("calificacion") instanceof List) {
                  relationcalificacion.setTarget((Node)mappers.get("calificacion_mapper").apply((Map)((List)in.get("calificacion")).get(0), status));
                }

                out.addRelation(relationcalificacion);
            }



            if (mappers.get("persona_mapper")!=null && in.get("conductor") != null){
                //direct
                var relationconductor = new Relation();
                relationconductor.setSourceNode("servicio");
                relationconductor.setTargetNode("persona");
                relationconductor.setDirectName("conductor");
                relationconductor.setReverseName("servicio");
                relationconductor.setSource(out);
                if (in.get("conductor") instanceof Map){
                  relationconductor.setTarget((Node)mappers.get("persona_mapper").apply((Map)in.get("conductor"), status));
                }else if (in.get("conductor") instanceof List) {
                  relationconductor.setTarget((Node)mappers.get("persona_mapper").apply((Map)((List)in.get("conductor")).get(0), status));
                }

                out.addRelation(relationconductor);
            }



            if (mappers.get("vehiculoservicio_mapper")!=null && in.get("vehiculosservicio") != null){
                //direct
                var relationvehiculosservicio = new Relation();
                relationvehiculosservicio.setSourceNode("servicio");
                relationvehiculosservicio.setTargetNode("vehiculoservicio");
                relationvehiculosservicio.setDirectName("vehiculosservicio");
                relationvehiculosservicio.setReverseName("servicio");
                relationvehiculosservicio.setSource(out);
                if (in.get("vehiculosservicio") instanceof Map){
                  relationvehiculosservicio.setTarget((Node)mappers.get("vehiculoservicio_mapper").apply((Map)in.get("vehiculosservicio"), status));
                }else if (in.get("vehiculosservicio") instanceof List) {
                  relationvehiculosservicio.setTarget((Node)mappers.get("vehiculoservicio_mapper").apply((Map)((List)in.get("vehiculosservicio")).get(0), status));
                }

                out.addRelation(relationvehiculosservicio);
            }




            if (mappers.get("persona_mapper")!=null && in.get("persona") != null){
                //reverse
                var relationpersona = new Relation();
                relationpersona.setSourceNode("persona");
                relationpersona.setTargetNode("servicio");
                relationpersona.setDirectName("servicios");
                relationpersona.setReverseName("persona");
                relationpersona.setTarget(out);
                if (in.get("persona") instanceof Map){
                  relationpersona.setSource((Node)mappers.get("persona_mapper").apply((Map)in.get("persona"), status));
                }else if (in.get("persona") instanceof List) {
                  relationpersona.setSource((Node)mappers.get("persona_mapper").apply((Map)((List)in.get("persona")).get(0), status));
                }

                out.addRelation(relationpersona);
            }



            if (mappers.get("vehiculo_mapper")!=null && in.get("vehiculo") != null){
                //reverse
                var relationvehiculo = new Relation();
                relationvehiculo.setSourceNode("vehiculo");
                relationvehiculo.setTargetNode("servicio");
                relationvehiculo.setDirectName("serviciosvehiculo");
                relationvehiculo.setReverseName("vehiculo");
                relationvehiculo.setTarget(out);
                if (in.get("vehiculo") instanceof Map){
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)in.get("vehiculo"), status));
                }else if (in.get("vehiculo") instanceof List) {
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)((List)in.get("vehiculo")).get(0), status));
                }

                out.addRelation(relationvehiculo);
            }


            return out;
        });
        mappers.put("calificacion_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("calificacion");

            if (in.containsKey("puntaje")){
              NodeProperty nodePropertypuntaje = new NodeProperty();
              nodePropertypuntaje.setPropertyName("puntaje");
              nodePropertypuntaje.setValue(in.get("puntaje"));
              out.addProperty(nodePropertypuntaje);
            }

            if (in.containsKey("comentarios")){
              NodeProperty nodePropertycomentarios = new NodeProperty();
              nodePropertycomentarios.setPropertyName("comentarios");
              nodePropertycomentarios.setValue(in.get("comentarios"));
              out.addProperty(nodePropertycomentarios);
            }


            if (mappers.get("servicio_mapper")!=null && in.get("servicio") != null){
                //reverse
                var relationservicio = new Relation();
                relationservicio.setSourceNode("servicio");
                relationservicio.setTargetNode("calificacion");
                relationservicio.setDirectName("calificacion");
                relationservicio.setReverseName("servicio");
                relationservicio.setTarget(out);
                if (in.get("servicio") instanceof Map){
                  relationservicio.setSource((Node)mappers.get("servicio_mapper").apply((Map)in.get("servicio"), status));
                }else if (in.get("servicio") instanceof List) {
                  relationservicio.setSource((Node)mappers.get("servicio_mapper").apply((Map)((List)in.get("servicio")).get(0), status));
                }

                out.addRelation(relationservicio);
            }


            return out;
        });
        mappers.put("rodamiento_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("rodamiento");

            if (in.containsKey("fechapago")){
              NodeProperty nodePropertyfechapago = new NodeProperty();
              nodePropertyfechapago.setPropertyName("fechapago");
              nodePropertyfechapago.setValue(in.get("fechapago"));
              out.addProperty(nodePropertyfechapago);
            }

            if (in.containsKey("fechainiciovigencia")){
              NodeProperty nodePropertyfechainiciovigencia = new NodeProperty();
              nodePropertyfechainiciovigencia.setPropertyName("fechainiciovigencia");
              nodePropertyfechainiciovigencia.setValue(in.get("fechainiciovigencia"));
              out.addProperty(nodePropertyfechainiciovigencia);
            }

            if (in.containsKey("fechafinvigencia")){
              NodeProperty nodePropertyfechafinvigencia = new NodeProperty();
              nodePropertyfechafinvigencia.setPropertyName("fechafinvigencia");
              nodePropertyfechafinvigencia.setValue(in.get("fechafinvigencia"));
              out.addProperty(nodePropertyfechafinvigencia);
            }

            if (in.containsKey("valor")){
              NodeProperty nodePropertyvalor = new NodeProperty();
              nodePropertyvalor.setPropertyName("valor");
              nodePropertyvalor.setValue(in.get("valor"));
              out.addProperty(nodePropertyvalor);
            }


            if (mappers.get("vehiculo_mapper")!=null && in.get("vehiculo") != null){
                //reverse
                var relationvehiculo = new Relation();
                relationvehiculo.setSourceNode("vehiculo");
                relationvehiculo.setTargetNode("rodamiento");
                relationvehiculo.setDirectName("rodamientos");
                relationvehiculo.setReverseName("vehiculo");
                relationvehiculo.setTarget(out);
                if (in.get("vehiculo") instanceof Map){
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)in.get("vehiculo"), status));
                }else if (in.get("vehiculo") instanceof List) {
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)((List)in.get("vehiculo")).get(0), status));
                }

                out.addRelation(relationvehiculo);
            }


            return out;
        });
        mappers.put("obligacionesoperacion_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("obligacionesoperacion");

            if (in.containsKey("tipoobligacion")){
              NodeProperty nodePropertytipoobligacion = new NodeProperty();
              nodePropertytipoobligacion.setPropertyName("tipoobligacion");
              nodePropertytipoobligacion.setValue(in.get("tipoobligacion"));
              out.addProperty(nodePropertytipoobligacion);
            }

            if (in.containsKey("fechainiciovigencia")){
              NodeProperty nodePropertyfechainiciovigencia = new NodeProperty();
              nodePropertyfechainiciovigencia.setPropertyName("fechainiciovigencia");
              nodePropertyfechainiciovigencia.setValue(in.get("fechainiciovigencia"));
              out.addProperty(nodePropertyfechainiciovigencia);
            }

            if (in.containsKey("fechafinvigencia")){
              NodeProperty nodePropertyfechafinvigencia = new NodeProperty();
              nodePropertyfechafinvigencia.setPropertyName("fechafinvigencia");
              nodePropertyfechafinvigencia.setValue(in.get("fechafinvigencia"));
              out.addProperty(nodePropertyfechafinvigencia);
            }

            if (in.containsKey("fecharealizacion")){
              NodeProperty nodePropertyfecharealizacion = new NodeProperty();
              nodePropertyfecharealizacion.setPropertyName("fecharealizacion");
              nodePropertyfecharealizacion.setValue(in.get("fecharealizacion"));
              out.addProperty(nodePropertyfecharealizacion);
            }


            if (mappers.get("vehiculo_mapper")!=null && in.get("vehiculo") != null){
                //reverse
                var relationvehiculo = new Relation();
                relationvehiculo.setSourceNode("vehiculo");
                relationvehiculo.setTargetNode("obligacionesoperacion");
                relationvehiculo.setDirectName("obligaciones");
                relationvehiculo.setReverseName("vehiculo");
                relationvehiculo.setTarget(out);
                if (in.get("vehiculo") instanceof Map){
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)in.get("vehiculo"), status));
                }else if (in.get("vehiculo") instanceof List) {
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)((List)in.get("vehiculo")).get(0), status));
                }

                out.addRelation(relationvehiculo);
            }


            return out;
        });
        mappers.put("turno_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("turno");

            if (in.containsKey("estado")){
              NodeProperty nodePropertyestado = new NodeProperty();
              nodePropertyestado.setPropertyName("estado");
              nodePropertyestado.setValue(in.get("estado"));
              out.addProperty(nodePropertyestado);
            }

            if (in.containsKey("fechainicio")){
              NodeProperty nodePropertyfechainicio = new NodeProperty();
              nodePropertyfechainicio.setPropertyName("fechainicio");
              nodePropertyfechainicio.setValue(in.get("fechainicio"));
              out.addProperty(nodePropertyfechainicio);
            }


            if (mappers.get("persona_mapper")!=null && in.get("conductor") != null){
                //reverse
                var relationconductor = new Relation();
                relationconductor.setSourceNode("persona");
                relationconductor.setTargetNode("turno");
                relationconductor.setDirectName("turnos");
                relationconductor.setReverseName("conductor");
                relationconductor.setTarget(out);
                if (in.get("conductor") instanceof Map){
                  relationconductor.setSource((Node)mappers.get("persona_mapper").apply((Map)in.get("conductor"), status));
                }else if (in.get("conductor") instanceof List) {
                  relationconductor.setSource((Node)mappers.get("persona_mapper").apply((Map)((List)in.get("conductor")).get(0), status));
                }

                out.addRelation(relationconductor);
            }



            if (mappers.get("vehiculo_mapper")!=null && in.get("vehiculo") != null){
                //reverse
                var relationvehiculo = new Relation();
                relationvehiculo.setSourceNode("vehiculo");
                relationvehiculo.setTargetNode("turno");
                relationvehiculo.setDirectName("turnos");
                relationvehiculo.setReverseName("vehiculo");
                relationvehiculo.setTarget(out);
                if (in.get("vehiculo") instanceof Map){
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)in.get("vehiculo"), status));
                }else if (in.get("vehiculo") instanceof List) {
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)((List)in.get("vehiculo")).get(0), status));
                }

                out.addRelation(relationvehiculo);
            }


            return out;
        });
        mappers.put("posicion_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("posicion");

            if (in.containsKey("latitud")){
              NodeProperty nodePropertylatitud = new NodeProperty();
              nodePropertylatitud.setPropertyName("latitud");
              nodePropertylatitud.setValue(in.get("latitud"));
              out.addProperty(nodePropertylatitud);
            }

            if (in.containsKey("longitud")){
              NodeProperty nodePropertylongitud = new NodeProperty();
              nodePropertylongitud.setPropertyName("longitud");
              nodePropertylongitud.setValue(in.get("longitud"));
              out.addProperty(nodePropertylongitud);
            }


            if (mappers.get("vehiculo_mapper")!=null && in.get("vehiculo") != null){
                //reverse
                var relationvehiculo = new Relation();
                relationvehiculo.setSourceNode("vehiculo");
                relationvehiculo.setTargetNode("posicion");
                relationvehiculo.setDirectName("posiciones");
                relationvehiculo.setReverseName("vehiculo");
                relationvehiculo.setTarget(out);
                if (in.get("vehiculo") instanceof Map){
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)in.get("vehiculo"), status));
                }else if (in.get("vehiculo") instanceof List) {
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)((List)in.get("vehiculo")).get(0), status));
                }

                out.addRelation(relationvehiculo);
            }


            return out;
        });
        mappers.put("incidente_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("incidente");

            if (in.containsKey("tipoincidente")){
              NodeProperty nodePropertytipoincidente = new NodeProperty();
              nodePropertytipoincidente.setPropertyName("tipoincidente");
              nodePropertytipoincidente.setValue(in.get("tipoincidente"));
              out.addProperty(nodePropertytipoincidente);
            }

            if (in.containsKey("fechaincidente")){
              NodeProperty nodePropertyfechaincidente = new NodeProperty();
              nodePropertyfechaincidente.setPropertyName("fechaincidente");
              nodePropertyfechaincidente.setValue(in.get("fechaincidente"));
              out.addProperty(nodePropertyfechaincidente);
            }

            if (in.containsKey("descripcion")){
              NodeProperty nodePropertydescripcion = new NodeProperty();
              nodePropertydescripcion.setPropertyName("descripcion");
              nodePropertydescripcion.setValue(in.get("descripcion"));
              out.addProperty(nodePropertydescripcion);
            }

            if (in.containsKey("latitud")){
              NodeProperty nodePropertylatitud = new NodeProperty();
              nodePropertylatitud.setPropertyName("latitud");
              nodePropertylatitud.setValue(in.get("latitud"));
              out.addProperty(nodePropertylatitud);
            }

            if (in.containsKey("longitud")){
              NodeProperty nodePropertylongitud = new NodeProperty();
              nodePropertylongitud.setPropertyName("longitud");
              nodePropertylongitud.setValue(in.get("longitud"));
              out.addProperty(nodePropertylongitud);
            }


            if (mappers.get("vehiculo_mapper")!=null && in.get("vehiculo") != null){
                //reverse
                var relationvehiculo = new Relation();
                relationvehiculo.setSourceNode("vehiculo");
                relationvehiculo.setTargetNode("incidente");
                relationvehiculo.setDirectName("incidentes");
                relationvehiculo.setReverseName("vehiculo");
                relationvehiculo.setTarget(out);
                if (in.get("vehiculo") instanceof Map){
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)in.get("vehiculo"), status));
                }else if (in.get("vehiculo") instanceof List) {
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)((List)in.get("vehiculo")).get(0), status));
                }

                out.addRelation(relationvehiculo);
            }


            return out;
        });
        mappers.put("barrio_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("barrio");

            if (in.containsKey("nombre")){
              NodeProperty nodePropertynombre = new NodeProperty();
              nodePropertynombre.setPropertyName("nombre");
              nodePropertynombre.setValue(in.get("nombre"));
              out.addProperty(nodePropertynombre);
            }


            if (mappers.get("zona_mapper")!=null && in.get("tarifa") != null){
                //reverse
                var relationtarifa = new Relation();
                relationtarifa.setSourceNode("zona");
                relationtarifa.setTargetNode("barrio");
                relationtarifa.setDirectName("barrios");
                relationtarifa.setReverseName("tarifa");
                relationtarifa.setTarget(out);
                if (in.get("tarifa") instanceof Map){
                  relationtarifa.setSource((Node)mappers.get("zona_mapper").apply((Map)in.get("tarifa"), status));
                }else if (in.get("tarifa") instanceof List) {
                  relationtarifa.setSource((Node)mappers.get("zona_mapper").apply((Map)((List)in.get("tarifa")).get(0), status));
                }

                out.addRelation(relationtarifa);
            }


            return out;
        });
        mappers.put("usuario_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("usuario");

            if (in.containsKey("estado")){
              NodeProperty nodePropertyestado = new NodeProperty();
              nodePropertyestado.setPropertyName("estado");
              nodePropertyestado.setValue(in.get("estado"));
              out.addProperty(nodePropertyestado);
            }

            if (in.containsKey("fecharegistro")){
              NodeProperty nodePropertyfecharegistro = new NodeProperty();
              nodePropertyfecharegistro.setPropertyName("fecharegistro");
              nodePropertyfecharegistro.setValue(in.get("fecharegistro"));
              out.addProperty(nodePropertyfecharegistro);
            }

            if (in.containsKey("usuario")){
              NodeProperty nodePropertyusuario = new NodeProperty();
              nodePropertyusuario.setPropertyName("usuario");
              nodePropertyusuario.setValue(in.get("usuario"));
              out.addProperty(nodePropertyusuario);
            }

            if (in.containsKey("correo")){
              NodeProperty nodePropertycorreo = new NodeProperty();
              nodePropertycorreo.setPropertyName("correo");
              nodePropertycorreo.setValue(in.get("correo"));
              out.addProperty(nodePropertycorreo);
            }

            if (in.containsKey("idusuario")){
              NodeProperty nodePropertyidusuario = new NodeProperty();
              nodePropertyidusuario.setPropertyName("idusuario");
              nodePropertyidusuario.setValue(in.get("idusuario"));
              out.addProperty(nodePropertyidusuario);
            }

            if (mappers.get("persona_mapper")!=null && in.get("persona") != null){
                //direct
                var relationpersona = new Relation();
                relationpersona.setSourceNode("usuario");
                relationpersona.setTargetNode("persona");
                relationpersona.setDirectName("persona");
                relationpersona.setReverseName("usuario");
                relationpersona.setSource(out);
                if (in.get("persona") instanceof Map){
                  relationpersona.setTarget((Node)mappers.get("persona_mapper").apply((Map)in.get("persona"), status));
                }else if (in.get("persona") instanceof List) {
                  relationpersona.setTarget((Node)mappers.get("persona_mapper").apply((Map)((List)in.get("persona")).get(0), status));
                }

                out.addRelation(relationpersona);
            }



            if (mappers.get("usuariocompania_mapper")!=null && in.get("companias") != null){
                //direct
                var relationcompanias = new Relation();
                relationcompanias.setSourceNode("usuario");
                relationcompanias.setTargetNode("usuariocompania");
                relationcompanias.setDirectName("companias");
                relationcompanias.setReverseName("usuario");
                relationcompanias.setSource(out);
                if (in.get("companias") instanceof Map){
                  relationcompanias.setTarget((Node)mappers.get("usuariocompania_mapper").apply((Map)in.get("companias"), status));
                }else if (in.get("companias") instanceof List) {
                  relationcompanias.setTarget((Node)mappers.get("usuariocompania_mapper").apply((Map)((List)in.get("companias")).get(0), status));
                }

                out.addRelation(relationcompanias);
            }



            return out;
        });
        mappers.put("usuariocompania_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("usuariocompania");

            if (in.containsKey("estado")){
              NodeProperty nodePropertyestado = new NodeProperty();
              nodePropertyestado.setPropertyName("estado");
              nodePropertyestado.setValue(in.get("estado"));
              out.addProperty(nodePropertyestado);
            }

            if (in.containsKey("fecharegistro")){
              NodeProperty nodePropertyfecharegistro = new NodeProperty();
              nodePropertyfecharegistro.setPropertyName("fecharegistro");
              nodePropertyfecharegistro.setValue(in.get("fecharegistro"));
              out.addProperty(nodePropertyfecharegistro);
            }


            if (mappers.get("empresa_mapper")!=null && in.get("compania") != null){
                //reverse
                var relationcompania = new Relation();
                relationcompania.setSourceNode("empresa");
                relationcompania.setTargetNode("usuariocompania");
                relationcompania.setDirectName("usuarios");
                relationcompania.setReverseName("compania");
                relationcompania.setTarget(out);
                if (in.get("compania") instanceof Map){
                  relationcompania.setSource((Node)mappers.get("empresa_mapper").apply((Map)in.get("compania"), status));
                }else if (in.get("compania") instanceof List) {
                  relationcompania.setSource((Node)mappers.get("empresa_mapper").apply((Map)((List)in.get("compania")).get(0), status));
                }

                out.addRelation(relationcompania);
            }



            if (mappers.get("usuario_mapper")!=null && in.get("usuario") != null){
                //reverse
                var relationusuario = new Relation();
                relationusuario.setSourceNode("usuario");
                relationusuario.setTargetNode("usuariocompania");
                relationusuario.setDirectName("companias");
                relationusuario.setReverseName("usuario");
                relationusuario.setTarget(out);
                if (in.get("usuario") instanceof Map){
                  relationusuario.setSource((Node)mappers.get("usuario_mapper").apply((Map)in.get("usuario"), status));
                }else if (in.get("usuario") instanceof List) {
                  relationusuario.setSource((Node)mappers.get("usuario_mapper").apply((Map)((List)in.get("usuario")).get(0), status));
                }

                out.addRelation(relationusuario);
            }


            return out;
        });
        mappers.put("zona_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("zona");

            if (in.containsKey("precio")){
              NodeProperty nodePropertyprecio = new NodeProperty();
              nodePropertyprecio.setPropertyName("precio");
              nodePropertyprecio.setValue(in.get("precio"));
              out.addProperty(nodePropertyprecio);
            }

            if (in.containsKey("zona")){
              NodeProperty nodePropertyzona = new NodeProperty();
              nodePropertyzona.setPropertyName("zona");
              nodePropertyzona.setValue(in.get("zona"));
              out.addProperty(nodePropertyzona);
            }

            if (in.containsKey("precionocturno")){
              NodeProperty nodePropertyprecionocturno = new NodeProperty();
              nodePropertyprecionocturno.setPropertyName("precionocturno");
              nodePropertyprecionocturno.setValue(in.get("precionocturno"));
              out.addProperty(nodePropertyprecionocturno);
            }

            if (mappers.get("barrio_mapper")!=null && in.get("barrios") != null){
                //direct
                var relationbarrios = new Relation();
                relationbarrios.setSourceNode("zona");
                relationbarrios.setTargetNode("barrio");
                relationbarrios.setDirectName("barrios");
                relationbarrios.setReverseName("tarifa");
                relationbarrios.setSource(out);
                if (in.get("barrios") instanceof Map){
                  relationbarrios.setTarget((Node)mappers.get("barrio_mapper").apply((Map)in.get("barrios"), status));
                }else if (in.get("barrios") instanceof List) {
                  relationbarrios.setTarget((Node)mappers.get("barrio_mapper").apply((Map)((List)in.get("barrios")).get(0), status));
                }

                out.addRelation(relationbarrios);
            }




            if (mappers.get("municipio_mapper")!=null && in.get("municipio") != null){
                //reverse
                var relationmunicipio = new Relation();
                relationmunicipio.setSourceNode("municipio");
                relationmunicipio.setTargetNode("zona");
                relationmunicipio.setDirectName("zonas");
                relationmunicipio.setReverseName("municipio");
                relationmunicipio.setTarget(out);
                if (in.get("municipio") instanceof Map){
                  relationmunicipio.setSource((Node)mappers.get("municipio_mapper").apply((Map)in.get("municipio"), status));
                }else if (in.get("municipio") instanceof List) {
                  relationmunicipio.setSource((Node)mappers.get("municipio_mapper").apply((Map)((List)in.get("municipio")).get(0), status));
                }

                out.addRelation(relationmunicipio);
            }


            return out;
        });
        mappers.put("vehiculoservicio_mapper", (in, status) -> {
            Node out = new Node();
            out.setId((UUID)in.get("id"));
            if (in.get("version") != null) {
                out.setVersion(((BigInteger) in.get("version")).intValue());
            }

            if (status!=null) {
                out.setStorageStatus(status);
            }

            out.setNodeName("vehiculoservicio");

            if (in.containsKey("estado")){
              NodeProperty nodePropertyestado = new NodeProperty();
              nodePropertyestado.setPropertyName("estado");
              nodePropertyestado.setValue(in.get("estado"));
              out.addProperty(nodePropertyestado);
            }

            if (in.containsKey("orden")){
              NodeProperty nodePropertyorden = new NodeProperty();
              nodePropertyorden.setPropertyName("orden");
              nodePropertyorden.setValue(in.get("orden"));
              out.addProperty(nodePropertyorden);
            }


            if (mappers.get("servicio_mapper")!=null && in.get("servicio") != null){
                //reverse
                var relationservicio = new Relation();
                relationservicio.setSourceNode("servicio");
                relationservicio.setTargetNode("vehiculoservicio");
                relationservicio.setDirectName("vehiculosservicio");
                relationservicio.setReverseName("servicio");
                relationservicio.setTarget(out);
                if (in.get("servicio") instanceof Map){
                  relationservicio.setSource((Node)mappers.get("servicio_mapper").apply((Map)in.get("servicio"), status));
                }else if (in.get("servicio") instanceof List) {
                  relationservicio.setSource((Node)mappers.get("servicio_mapper").apply((Map)((List)in.get("servicio")).get(0), status));
                }

                out.addRelation(relationservicio);
            }



            if (mappers.get("vehiculo_mapper")!=null && in.get("vehiculo") != null){
                //reverse
                var relationvehiculo = new Relation();
                relationvehiculo.setSourceNode("vehiculo");
                relationvehiculo.setTargetNode("vehiculoservicio");
                relationvehiculo.setDirectName("vehiculosservicio");
                relationvehiculo.setReverseName("vehiculo");
                relationvehiculo.setTarget(out);
                if (in.get("vehiculo") instanceof Map){
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)in.get("vehiculo"), status));
                }else if (in.get("vehiculo") instanceof List) {
                  relationvehiculo.setSource((Node)mappers.get("vehiculo_mapper").apply((Map)((List)in.get("vehiculo")).get(0), status));
                }

                out.addRelation(relationvehiculo);
            }


            return out;
        });
    }

}
