package bee.taxiservices.services.base ;

import bee.persistence.GraphQLTypes;
import bee.datastorage.graphql.resolver.ListOperationResolver;
import bee.datastorage.graphql.DataStorageExecutor;
import bee.datastorage.graphql.DataStorageOperation;
import bee.datastorage.graphql.resolver.OperationResolver;
import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.session.ExecutionContext;
import graphql.schema.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import bee.datastorage.model.EStorageStatus;
import java.util.function.BiFunction;

import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;

public abstract class obligacionesoperacion_actualizarBase extends DataStorageExecutor< Node > {
    @Override
    public  Node  execute(DataFetchingEnvironment dataFetchingEnvironment, ExecutionContext executionContext)  throws BusinessException {

        EStorageStatus status = null;

        status = EStorageStatus.TOUPDATE;

        BiFunction<Map,EStorageStatus, Object> mapper = Mappers.mappers.get("obligacionesoperacion_mapper"); //

Node input = (Node) mapper.apply((Map)dataFetchingEnvironment.getArgument("input") , status);

Node dataResponse = this.execute( input, executionContext);

    if (dataResponse == null){
        return null;
    }
        return dataResponse;
    }

    public abstract Node execute(Node input, ExecutionContext executionContext)  throws BusinessException;

    @Override
    public GraphQLFieldDefinition buildOperationType(DataStorageOperation operation) {
        GraphQLFieldDefinition.Builder type = newFieldDefinition()
                .name(operation.getName());

        type.argument(GraphQLArgument.newArgument().name("input").type(GraphQLTypeReference.typeRef("obligacionesoperacionInput")).build());

        type.type(GraphQLTypeReference.typeRef("obligacionesoperacion"));
        type.dataFetcher(OperationResolver.INSTANCE);

        return type.build();
    }
}
