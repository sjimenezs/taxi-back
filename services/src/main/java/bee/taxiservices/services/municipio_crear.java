package bee.taxiservices.services;

import bee.datastorage.IDataStorageFacade;
import bee.datastorage.model.storage.Node;
import bee.taxiservices.services.base.*;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;

import java.util.List;
import java.util.UUID;

public class municipio_crear extends municipio_crearBase {
    @Override
    public Node execute(Node input, ExecutionContext executionContext)  throws BusinessException{
        IDataStorageFacade facade = Registry.getInstance(IDataStorageFacade.class);
return facade.persistFromService( input ,executionContext);

    }
}
