package bee.taxiservices.business;

import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.session.ExecutionContext;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface ITaxiBusiness {
    UUID asociarConductor(Node input, ExecutionContext ec) throws BusinessException;

    UUID servicioAsociarConductor(Node input, ExecutionContext ec) throws BusinessException;

    UUID crearUsuario(Node input, ExecutionContext ec) throws BusinessException;

    List<Node> consultarIncidentes(Node input, ExecutionContext executionContext);

    List<Node> consultarServicios(Node input, ExecutionContext executionContext);

    List<Node> consultarVehiculosDisponibles(Node input, ExecutionContext executionContext);

    UUID crearPosicion(Node input, ExecutionContext ec) throws BusinessException;
}
