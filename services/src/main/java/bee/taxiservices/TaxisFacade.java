package bee.taxiservices;

import bee.datastorage.model.storage.Node;
import bee.error.BusinessException;
import bee.registry.Registry;
import bee.session.ExecutionContext;
import bee.taxiservices.business.ITaxiBusiness;
import taxi.common.facade.ITaxisFacade;

import java.util.List;
import java.util.UUID;

public class TaxisFacade implements ITaxisFacade {

    @Override
    public UUID asociarConductor(Node input, ExecutionContext ec) throws BusinessException {
        var business = Registry.getInstance(ITaxiBusiness.class);
        return business.asociarConductor(input,ec);
    }

    @Override
    public UUID servicioAsociarConductor(Node input, ExecutionContext ec) throws BusinessException {
        var business = Registry.getInstance(ITaxiBusiness.class);
        return business.servicioAsociarConductor(input,ec);
    }

    @Override
    public UUID crearUsuario(Node input, ExecutionContext ec) throws BusinessException {
        var business = Registry.getInstance(ITaxiBusiness.class);
        return business.crearUsuario(input,ec);
    }

    @Override
    public UUID crearPosicion(Node input, ExecutionContext ec) throws BusinessException {
        var business = Registry.getInstance(ITaxiBusiness.class);
        return business.crearPosicion(input,ec);
    }

    @Override
    public List<Node> consultarIncidentes(Node input, ExecutionContext executionContext) throws BusinessException {
        var business = Registry.getInstance(ITaxiBusiness.class);
        return business.consultarIncidentes(input, executionContext);
    }

    @Override
    public List<Node> consultarServicios(Node input, ExecutionContext executionContext) throws BusinessException {
        var business = Registry.getInstance(ITaxiBusiness.class);
        return business.consultarServicios(input, executionContext);
    }

    @Override
    public List<Node> consultarVehiculosDisponibles(Node input, ExecutionContext executionContext) throws BusinessException {
        var business = Registry.getInstance(ITaxiBusiness.class);
        return business.consultarVehiculosDisponibles(input, executionContext);
    }
}
