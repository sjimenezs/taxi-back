package bee.taxi;

import bee.configuracion.Configuracion;
import bee.serviceregistry.Modules;

import java.io.File;

public class Main {
    public static void main(String... args) {
        try {

            Configuracion.load(new File(args[0]));
            String[] modulos = null;

            Modules.init(Configuracion.getConf("modulos", String[].class));


        } catch (Exception e) {
            e.printStackTrace();
            Throwable t = e;
            while (t.getCause() != null) {
                t = e.getCause();
                t.printStackTrace();
            }
        }
    }
}
